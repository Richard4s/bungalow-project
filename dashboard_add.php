<?php

require_once 'core/init.php';
require_once 'classes/Upload.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

$stmt = $conn->stmt_init();

if(array_key_exists('add_item_submit', $_POST)){
    $name = $_POST['add_item_name'];

    $description = $_POST['add_item_description'];
    
    $category = $_POST['optionguys'];

    $fileupload = $_FILES['add_item_image'];

    $branch = $_POST['add_item_check'];

    $error = false; $message = array(); $success = false;

    $date = date('Y-m-d H:i:s');

    $branchVI = '';
    $branchIKEJA = '';
//||$fileupload['name']==''
    if(empty($name)||empty($description)||$category==0||count($branch)==0){
        $error=true;
        $message[] = 'Fill up the fields';    
    }else{
        //filter branch details
        $branchcount = count($branch);
        for($i = 0;$i < $branchcount; $i++){
            if($branch[$i] == 1){
                $branchVI = $_POST['add_item_price_vi'];
                if(empty($branchVI)){$error = true; $message[] = 'Fill up the fields';}
            }elseif($branch[$i] == 2){
                $branchIKEJA = $_POST['add_item_price_ikeja'];
                if(empty($branchIKEJA)){$error = true; $message[] = 'Fill up the fields';}
            }
        }

            //upload image
            
        //continue insertion
        $sql = "INSERT INTO products (ProductName, ProductDescription, ProductCategoryId, DateCreated)
        VALUES (?,?,?,?)";
        if($stmt->prepare($sql)){
            $stmt->bind_param('ssis', $name, $description, $category, $date);
            $stmt->execute();
            if($stmt->affected_rows > 0){
                $id = $stmt->insert_id;
            }else{
                $error = true;
                $message[] = 'Couldnt insert record into database';
            }
        }
        $stmt->free_result();

        
        //add to branch_product table
        for($i = 0;$i < $branchcount; $i++){
            if($branch[$i] == 1){
                $sql = "INSERT INTO branch_product (ProductId, BranchId, Price)
                VALUES (?,?,?)";
                if($stmt->prepare($sql)){
                    $stmt->bind_param('iii', $id, $category, $branchVI);
                    $stmt->execute();
                    if($stmt->affected_rows > 0){
                    
                    }else{
                        $error = true;
                        $message[] = 'Couldnt insert record into database';
                    }
                }   
                $stmt->free_result();     
            }elseif($branch[$i] == 2){
                $sql = "INSERT INTO branch_product (ProductId, BranchId, Price)
                VALUES (?,?,?)";
                if($stmt->prepare($sql)){
                    $stmt->bind_param('iii', $id, $category, $branchIKEJA);
                    $stmt->execute();
                    if($stmt->affected_rows > 0){
                        
                    }else{
                        $error = true;
                        $message[] = 'Couldnt insert record into database';
                    }
                    $stmt->free_result();
                }
            }

            
        }
            
        if(!$error){
            $success = true;
            $message[] = 'Record inserted';
        }
    }
}

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Add Products</h2>
                <ul>
                <?php 
                    if($_POST && $error){
                        foreach($message as $messages){ ?>
                            <li><?= $messages; ?></li>
                        <?php }
                    }elseif($_POST && $success){
                        foreach($message as $messages){ ?>
                            <li><?= $messages; ?></li>
                        <?php }
                    }{

                    }
                ?>
                </ul>
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <form class="mdform" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
                <div class="form-group md-form-group">

                    <label for="form1-email">Name of Item</label>
                    <input name="add_item_name" type="text" class="form-control">
                </div>
                <!-- <div class="form-group md-form-group">
                    <label for="form1-pass">Price of Item</label>
                    <input name="add_item_price" type="text" class="form-control">
                </div> -->
                <div class="form-group md-form-group">
                    <input type="file" name="add_item_image" id="imageChecker" accept=".jpg, .jpeg, .png"/>
                </div>
                <div class="form-group md-form-group">
                    <label for="form1-pass"></label>
                    <textarea name="add_item_description" class="form-control" placeholder="Item Description"></textarea>
                </div>
<!--                <div class="mdl-textfield mdl"-->
                <div class="form-group md-form-group">
                    <h4>Select Product Category</h4>
                    <select name="optionguys">
                        <option value="0"><span class="label-text">Select One:</span></option>
                        <?php
                            $sql = "SELECT a.Id, a.CategoryName, b.ProductCategoryName FROM category a, subproductcategory b WHERE a.SubProductCategoryId = b.Id";
                            if($stmt->prepare($sql)){
                                $stmt->execute();
                                $stmt->bind_result($a, $b, $c);
                                $stmt->store_result();
                                $numrows = $stmt->num_rows();
                                if($numrows > 0){
                                    while($stmt->fetch()){ ?>
                                <option value="<?= $a; ?>"><span class="label-text"><?= $b.'('.$c.')'; ?></span></option>     
                                <?php }
                                }
                            }
                        ?>
                    </select>
                </div>


                <div class="form-group checkbox">
                    Branch
                    <div class="row">
                    <div class="form-group col-md-3">
                        <input type="checkbox" name="add_item_check[]" value="1" class="icheck" > <span class="label-text">VI</span>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="form1-pass">Price of Item</label>
                        <input name="add_item_price_vi" type="text" class="form-control">
                    </div>
                    </div>

                    <div class="row">
                    <div class="form-group col-md-3">
                        <input type="checkbox" name="add_item_check[]" value="2" class="icheck" > <span class="label-text">Ikeja</span>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="form1-pass">Price of Item</label>
                        <input name="add_item_price_ikeja" type="text" class="form-control">
                    </div>
                </div>
                </div>
                <button name="add_item_submit" type="submit" class="btn btn-info">Submit</button>

            </form>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>

<?php
////echo __DIR__;
//
//if(isset($_POST['add_item_submit']) && isset($_FILES['add_item_image']['name'])) {
//
//
//                        if(Input::exists()) {
//                            //if (Token::check(Input::get('token'))) {
//
//                           // echo 'I have been run';
//                            //var_dump($_POST);
//
//                            $validate = new Validate();
//                            $validation = $validate->check($_POST, [
//                                'add_item_name' => [
//                                    'required' => 'true',
//                                    'min' => 2,
//                                    'max' => 50
//                                ],
//                                'add_item_price' => [
//                                    'required' => 'true',
//                                    'min' => 2,
//                                    'max' => 50
//                                ],
//                                'add_item_description' => [
//                                    'required' => true,
//                                    'min' => 5
//                                ],
//                                'optionguys' => [
//                                        'required' => true
//                                ],
//                            ]);
//
//                            if ($validation->passed()) {
//
//                                try {
//
//                                    $uploadedFile = $_FILES['add_item_image']['name'];
//                                    $ext = pathinfo($uploadedFile);
//                                    $newname = $_POST['add_item_name'] . '.' . $ext['extension'];
//
//                                    if(move_uploaded_file($_FILES['add_item_image']['tmp_name'], 'uploads/' . $newname)) {
//                                        echo 'Ok';
//                                        $resize = new ResizeImage("uploads/".$newname);
//                                        $resize->resizeTo(252, 252);
//                                        $resize->saveImage("uploads/".$newname);
//                                    } else {
//                                        echo 'Something went wrong.';
//                                    }
//
//
//
//
//                                    $options = input::get('optionguys');
//                                    $myImage = DB::getInstance()->insert($options, [
//                                        'product_name' => Input::get('add_item_name'),
//                                        'product_price' => Input::get('add_item_price'),
//                                        'product_description' => Input::get('add_item_description'),
//                                        'product_image' => $newname,
//                                        'created_at' => date('Y-m-d H:i:s')
//                                    ]);
//                                    echo "Product has been added successfully!";
//
//                                } catch (Exception $e) {
//                                    die($e->getMessage());
//                                }
//                            } else {
//                                foreach ($validation->errors() as $error) {
//                                    echo 'Something went wrong.' . $error;
//
//                                }
//                            }
//                            //}
//                        }
//
//}


?>
