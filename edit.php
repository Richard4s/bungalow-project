
<?php

require_once 'core/init.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';

if(!isset($_GET['id']) && isset($_GET['category'])) {
    echo '<script>window.location="include/errors/404.php"</script>';
} else {
    $editingid = $_GET['id'];
    $category = $_GET['category'];

    $edit = DB::getInstance()->query("SELECT * FROM {$category} WHERE id={$editingid}");

    foreach($edit->results() as $editvalue) {
        $editId = $editvalue->id;
        $editName = $editvalue->product_name;
        $editPrice = $editvalue->product_price;
        $editDescription = $editvalue->product_description;
        $editImage = $editvalue->product_image;
    }

}

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Edit Products
                    <?php


                    if(isset($_POST['add_item_submit'])) {

                        if(Input::exists()) {
                            //if (Token::check(Input::get('token'))) {

                            // echo 'I have been run';
                            //var_dump($_POST);

                            $validate = new Validate();
                            $validation = $validate->check($_POST, [
                                'add_item_name' => [
                                    'required' => 'true',
                                    'min' => 2,
                                    'max' => 50
                                ],
                                'add_item_price' => [
                                    'required' => 'true',
                                    'min' => 2,
                                    'max' => 50
                                ],
                                'add_item_description' => [
                                    'required' => true,
                                    'min' => 5
                                ]
                            ]);

                            if ($validation->passed()) {

                                try {

                                    if(isset($_FILES['add_item_image']['name'])) {
                                        if(!empty($_FILES['add_item_image']['name'])) {
                                            $uploadedFile = $_FILES['add_item_image']['name'];
                                            $ext = pathinfo($uploadedFile);
                                            $newname = $_POST['add_item_name'] . '.' . $ext['extension'];

                                            if(move_uploaded_file($_FILES['add_item_image']['tmp_name'], 'uploads/' . $newname)) {
                                                echo 'Ok';
                                                $resize = new ResizeImage("uploads/".$newname);
                                                $resize->resizeTo(252, 252);
                                                $resize->saveImage("uploads/".$newname);
                                            } else {
                                                echo 'Something went wrong.';
                                            }
                                        }

                                    } else {
                                        echo $newname = $editImage;
                                    }


                                    if(!isset($_POST['optionguys'])) {
                                        if(empty($_POST['optionguys'])) {
                                            $options = $category;
                                        }
                                    } else{
                                        $options = input::get('optionguys');
                                    }

                                    $myImage = DB::getInstance()->update($options, $editId, [
                                        'product_name' => Input::get('add_item_name'),
                                        'product_price' => Input::get('add_item_price'),
                                        'product_description' => Input::get('add_item_description'),
                                        'product_image' => $newname,
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ]);
                                    echo "Product has been added successfully!";

                                } catch (Exception $e) {
                                    die($e->getMessage());
                                }
                            } else {
                                foreach ($validation->errors() as $error) {
                                    echo 'Something went wrong.' . $error;

                                }
                            }
                            //}
                        }

                    }
                    ?>
                </h2>
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <form class="mdform" method="post" action="" enctype="multipart/form-data">
                <div class="form-group md-form-group">


                    <input name="add_item_name" placeholder="Name of Item" value="<?php echo $editName ?>" type="text" class="form-control">
                </div>
                <div class="form-group md-form-group">
                    <input name="add_item_price"  placeholder="Price of Item" value="<?php echo $editPrice ?>" type="text" class="form-control">
                </div>
                <div class="form-group md-form-group">
                    <img class="img-responsive" src="uploads/<?php echo $editImage ?>" />
                    <span><br><h4>Change this image?</h4></span>
                    <input type="file" name="add_item_image" id="imageChecker" accept=".jpg, .jpeg, .png"/>
                </div>
                <div class="form-group md-form-group">
                    <label for="form1-pass"></label>
                    <textarea name="add_item_description" class="form-control" placeholder="Item Description"><?php echo $editDescription ?></textarea>
                </div>
                <!--                <div class="mdl-textfield mdl"-->
                <div class="form-group md-form-group">
                    <h4>Change Category?</h4>
                    <select id="mygroup" name="optionguys">
                        <option disabled selected><span class="label-text">Change Category?</span></option>
                        <option value="bungalow_appetizers"><span class="label-text">Bungalow Appetizers</span></option>
                        <option value="bungalow_salad"> <span class="label-text">Bungalow Salads</span></option>
                        <option value="sandwiches"><span class="label-text">Sandwiches</span></option>
                        <option value="burger_hotdogs"><span class="label-text">Burgers & Hotdogs</span></option>
                        <option value="pizza"><span class="label-text">Pizza</span></option>
                        <option value="pasta"> <span class="label-text">Pasta</span></option>
                        <option value="beef_chicken"><span class="label-text">Beef & Chicken Platters</span></option>
                        <option value="char_grilled"> <span class="label-text">Char-Grilled Steaks</span></option>
                        <option value="nigerian_menu"><span class="label-text">Nigerian Menu</span></option>
                        <option value="seafood"> <span class="label-text">Seafood</span></option>
                        <option value="lite_section"> <span class="label-text">Lite Section</span></option>
                        <option value="salted_crepes"> <span class="label-text">Salted Crepes</span></option>
                        <option value="sweet_crepes"> <span class="label-text">Sweet Crepes</span></option>
                        <option value="drinks"> <span class="label-text">Drinks</span></option>
                    </select>
                </div>

                <button name="add_item_submit" type="submit" class="btn btn-info">Submit</button>
            </form>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>


