
<div id="mobile-menu">
<div class="mobile-menu-inner">
  <ul>
  <li>
     <div class="mm-search">
     <div id="search1">
          <div class="input-group">
            <div class="input-group-btn">
                  <button id="mm-submit-button-search-header" class="btn btn-default">
                  <i class="fa fa-search"></i>
                  </button>
            </div>
             <input id="srch-term" class="form-control simple" type="text" name="search_mm" maxlength="70" value="" placeholder="Search here..">
          </div>
      </div>
     </div>
  </li>

      <li><a href="index.php">Home</a></li>
            <li><a href="bungalow_appetizer.php">Appetizers</a></li>
            <li><a href="bungalow_salad.php">Salad</a></li>
            <li><a href="sandwiches.php">Sandwiches</a></li>
            <li><a href="burger_hotdog.php">Burgers & Hotdogs</a></li>
        </ul>
   <div class="top-links">
    <ul class="links">
        <li><a href="register.php" title="My Account">My Account</a></li>
        <li class="last"><a href="login.php">Login</a></li>
    </ul>
  </div>
</div>
</div><!-- mobile-menu -->
