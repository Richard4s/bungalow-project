<?php

require_once 'core/init.php';
require_once 'classes/Upload.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

$stmt = $conn->stmt_init();

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Manage Featured Products</h2>
                
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <table border="0" class="table">
                <tr>
                    <th>Category</th>
                    <th>&nbsp;</th>
                </tr>
                <?php 
                $sql = "SELECT a.Id, a.ProductName, b.CategoryName, c.ProductCategoryName 
                FROM products a, category b, subproductcategory c
                WHERE a.ProductCategoryId = b.Id AND b.SubProductCategoryId = c.Id
                AND a.Id IN (SELECT ProductId from featured)";
                $stmt->prepare($sql);
                $stmt->bind_result($a, $b, $c, $d);
                $stmt->execute();  
                $stmt->store_result();
                if(!$stmt->num_rows > 0){
                    echo 'No Featured Product';
                }else{ 
                
                    while($stmt->fetch()){ ?>
                        <tr>
                            <td><?= ucfirst($b).'- '.$c.' ('.$d.')'; ?></td>
                            <td><a href="dashboard_delete_featured.php?id=<?= $a; ?>" class="btn btn-danger">Unmark as featured product</a></td>
                        </tr>
                <?php   }
                    
                }
                ?>
            </table>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>
