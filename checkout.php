<?php

    require_once 'core/init.php';
    require_once 'template/header.php';


    ?>


    <div id="mgkquickview">
        <div id="magikloading" style="display:none;text-align:center;margin-top:400px;"><img src="catalog/view/theme/kabriodemo2/image/loading.gif" alt="loading">
        </div></div>
    <div class="breadcrumbs">
    </div>

    <section class="main-container col1-layout">
        <div class="main" id="content">
            <div class="container">


<?php

    $retrieval = DB::getInstance()->query("SELECT * FROM {$_SESSION['tableName']} WHERE id = {$_SESSION['foodId']}");
    if(!$retrieval->results()) {
        echo 'No products are available here yet. You can contact us to enquire <a href="contact_us.php">here.</a>';
    } else {
        foreach($retrieval->results() as $retrieves)  {
            echo '<div class="row">

                    <div class="col-main">
                        <div class="product-view">
                            <div class="product-essential">

                                <div class="product-img-box col-lg-4 col-sm-5 col-xs-12">
                                    <div class="product-image">
                                        <div class="product-full"> <img id="product-zoom" src="uploads/'.$retrieves->product_image.'" data-zoom-image="image/cache/catalog/Restaurant/product9-700x850.jpg"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-shop col-lg-8 col-sm-7 col-xs-12">
                                    <div class="product-name">
                                        <h1>'.$retrieves->product_name.'</h1>
                                    </div>
                                    
                                    <div class="price-block">
                                        <div class="price-box">
                                            <p class="regular-price"><span class="price">'.$retrieves->product_price.'</span></p>

                                            <p class="availability in-stock"><span>In Stock</span></p>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li>Brand: <a href="index98fa.html?route=product/manufacturer/info&amp;manufacturer_id=8">'.$_SESSION['tableName'].'</a></li>

                                    </ul>
                                    <div id="product">
                                        <div class="add-to-box">

                                            <div class="add-to-cart">
                                                <label class="control-label" for="input-quantity">Qty</label>
                                                <div class="pull-left">
                                                    <div class="custom pull-left">

                                                        <button class="reduced items-count" onclick="var result = document.getElementById(\'qty\'); var qty = result.value; if( !isNaN( qty ) && qty > 0 ) result.value--;return false;" type="button">
                                                            <i class="fa fa-minus"> </i>
                                                        </button>
                                                        <input type="text" name="quantity" value="1" size="2" id="qty" class="input-text qty" maxlength="12"/>
                                                        <button class="increase items-count" onclick="var result = document.getElementById(\'qty\'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" type="button">
                                                            <i class="fa fa-plus"> </i>
                                                        </button>

                                                        <input type="hidden" name="product_id" value="1009" />
                                                    </div>
                                                </div>

                                                <div class="pull-left">
                                                    <button type="button" id="button-cart" data-loading-text="Loading..." class="button btn-cart"><span>Checkout</span></button>
                                                </div>

                                            </div>


                                            </div>

                                        </div>
                                        
                                        <div class="addthis_toolbox addthis_default_style" data-url="indexdf3d.html?route=product/product&amp;product_id=1009"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                                        

                                    </div>
                                </div>





                            </div>
                        </div>
                    </div>';
        }
    }

?>




















                    <div class="product-collateral col-lg-12 col-sm-12 col-xs-12">
                        <div class="add_info">


                            <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                                <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
                                <li><a href="#tab-review" data-toggle="tab">Reviews (0)</a></li>

                            </ul>
                            <div id="productTabContent" class="tab-content">
                                <div class="tab-pane active" id="tab-description"><p>Apples are a natural source of fibre and are fat free. They contain anti-oxidants and polynutrients. These Washington Apples are crisp, red, smooth-skinned, juicy fruits. The flesh of that apple tastes crispy and very juicy with a sugary-sweet flavor. Product image shown is for representation purpose only, the actually product may vary based on season, produce & availability.</p></div>
                                <div class="tab-pane" id="tab-review">
                                    <form class="form-horizontal" id="form-review">
                                        <div id="review"></div>
                                        <h2>Write a review</h2>
                                        <div class="form-group required">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="input-name">Your Name</label>
                                                <input type="text" name="name" value="" id="input-name" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="input-review">Your Review</label>
                                                <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                                <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <div class="col-sm-12">
                                                <label class="control-label">Rating</label>
                                                &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                                <input type="radio" name="rating" value="1" />
                                                &nbsp;
                                                <input type="radio" name="rating" value="2" />
                                                &nbsp;
                                                <input type="radio" name="rating" value="3" />
                                                &nbsp;
                                                <input type="radio" name="rating" value="4" />
                                                &nbsp;
                                                <input type="radio" name="rating" value="5" />
                                                &nbsp;Good</div>
                                        </div>
                                        <div class="buttons clearfix">
                                            <div class="pull-right">
                                                <button type="button" id="button-review" data-loading-text="Loading..." class="btn btn-primary">Continue</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>


                        </div><!-- col-sm-12 wow bounceInUp animated animated -->
                    </div><!-- product-collateral -->

                </div>
            </div>
        </div>
    </section>




    <div class="container">
        <div class="related-pro">
            <div class="slider-items-products">
                <div class="related-block">
                    <div id="related-products-slider" class="product-flexslider hidden-buttons">
                        <div class="home-block-inner">
                            <div class="new_title">
                                <h2>Related Products</h2>
                                <div class="line">
                                    <div class="star"></div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-items slider-width-col4 products-grid block-content">
                            <!-- Item -->
                            <div class="item">
                                <div class="item-inner">

                                    <div class="item-img">
                                        <div class="item-img-info">
                                            <a class="product-image" href="indexdc2d.html?route=product/product&amp;product_id=1001" title="Thanksgiving Leftover Cornucopia">
                                                <img src="image/cache/catalog/Restaurant/product1-700x850.jpg" alt="Thanksgiving Leftover Cornucopia" title="Thanksgiving Leftover Cornucopia"/>
                                            </a>
                                            <div class="sale-label sale-top-right">Sale</div>

                                        </div>
                                    </div>
                                    <div class="item-info">
                                        <div class="info-inner">
                                            <div class="item-title">
                                                <a title="Thanksgiving Leftover Cornucopia" href="indexdc2d.html?route=product/product&amp;product_id=1001">
                                                    Thanksgiving Leftover Cor...                              </a>
                                            </div>
                                            <div class="rating">
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                            </div><!-- rating -->

                                            <div class="item-content">

                                                <div class="item-price">
                                                    <div class="price-box">

                                                        <p class="old-price"><span class="price">$100.00</span></p>
                                                        <p class="special-price"><span class="price">$80.00</span></p>
                                                    </div>
                                                </div>
                                                <div class="action">
                                                    <ul class="add-to-links">
                                                        <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1001');"><i class="fa fa-shopping-cart"></i></button></li>
                                                        <li>
                                                            <a onclick="wishlist.add('1001');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                        </li>
                                                        <li>
                                                            <a class="link-compare"  onclick="compare.add('1001');"><i class="fa fa-random"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>  <!-- End Item info -->
                                </div>  <!-- End  Item inner-->
                            </div> <!-- End Item -->
                            <!-- Item -->
                            <div class="item">
                                <div class="item-inner">

                                    <div class="item-img">
                                        <div class="item-img-info">
                                            <a class="product-image" href="indexf8e0.html?route=product/product&amp;product_id=1005" title="Rolled chicken">
                                                <img src="image/cache/catalog/Restaurant/product5-700x850.jpg" alt="Rolled chicken" title="Rolled chicken"/>
                                            </a>

                                        </div>
                                    </div>
                                    <div class="item-info">
                                        <div class="info-inner">
                                            <div class="item-title">
                                                <a title="Rolled chicken" href="indexf8e0.html?route=product/product&amp;product_id=1005">
                                                    Rolled chicken                              </a>
                                            </div>
                                            <div class="rating">
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                            </div><!-- rating -->

                                            <div class="item-content">

                                                <div class="item-price">
                                                    <div class="price-box">
                                                        <p class="regular-price"><span class="price">$100.00</span></p>
                                                    </div>
                                                </div>
                                                <div class="action">
                                                    <ul class="add-to-links">
                                                        <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1005');"><i class="fa fa-shopping-cart"></i></button></li>
                                                        <li>
                                                            <a onclick="wishlist.add('1005');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                        </li>
                                                        <li>
                                                            <a class="link-compare"  onclick="compare.add('1005');"><i class="fa fa-random"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>  <!-- End Item info -->
                                </div>  <!-- End  Item inner-->
                            </div> <!-- End Item -->

                        </div>
                    </div>    <!-- featured -->
                </div>
            </div>    <!-- slider Item products -->
        </div>
    </div>

<?php

require_once 'template/footer.php';
require_once 'template/mobile_menu.php';
require_once 'template/scripts.php';

?>