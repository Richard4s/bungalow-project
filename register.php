<?php

  require_once 'core/init.php';
  require_once 'template/header.php';

  if(Input::exists()) {
  	//if(Token::check(Input::get('token'))) {

  		//echo 'I have been run';
      var_dump($_POST);

      $validate = new Validate();
  		$validation = $validate->check($_POST, [
  			'firstname' => [
  				'required' => 'true',
  				'min' => 2,
  				'max' => 50
  			],
        'lastname' => [
  				'required' => 'true',
  				'min' => 2,
  				'max' => 50
  			],
  			'email' => [
  				'required' => true,
  				'min' => 5
  			],
        'telephone' => [
  				'required' => true,
  				'min' => 5
  			],
  			'password' => [
  				'required' => true,
  				'min' => 6
  			],
  			'confirm_password' => [
  				'required' => true,
  				'matches' => 'password'
  			]
  		]);

  		if($validation->passed()){
  			// Session::flash('success', 'You registered successfully');
  			// header('Location: index.php');

  			$user = new User();
  			//$salt = Hash::salt(32);

  			try {

  				$user->create([
  					'FirstName' => Input::get('firstname'),
                    'LastName' => Input::get('lastname'),
                    'Telephone' => Input::get('telephone'),
  					'Password' => Hash::create(Input::get('password')),
                    'Email' => Input::get('email'),
  					'DateJoined' => date('Y-m-d H:i:s'),
  					'AccessLevel' => 1
  				]);

  				Session::flash('home', 'You have been registered and can now log in!');
  				echo "<script>window.location = 'login.php'</script>";

  			} catch(Exception $e) {
  				die($e->getMessage());
  			}
  		} else{
  			foreach($validation->errors() as $error) {
  				echo 'Something went wrong.'. $error;

  			}
  		}
  	//}
  }

 ?>



<div class="offer-box">
  <div class="container">
    <div class="row">
      <div class="col-md-10"><span> enjoy our food delivery</span></div>
    </div>
  </div>
</div>
<div id="mgkquickview">
<div id="magikloading" style="display:none;text-align:center;margin-top:400px;"><img src="catalog/view/theme/kabriodemo2/image/loading.gif" alt="loading">
</div></div><div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
                        <li>                            <a href="index9328.html?route=common/home">Home</a>
                          </li>
                        <li><span>/</span>                            <a href="indexe223.html?route=account/account">Account</a>
                          </li>
                        <li><span>/</span>                            <strong>Register</strong>            </li>

          </ul>
        </div>
      </div>
    </div>
</div>
<div class="main-container col2-right-layout">
<div class="main container">
    <div class="row">                <div id="content" class="col-sm-9">
      <div class="col-main">
      <div class="my-account">
            <div class="page-title">
        <h2> Register Account</h2>
      </div>
      <p>If you already have an account with us, please login at the <a href="login.php">login page</a>.</p>
      <form action="" method="post" class="form-horizontal">
        <fieldset id="account">
          <legend>Your Personal Details</legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-firstname">First Name</label>
            <div class="col-sm-10">
              <input type="text" name="firstname" value="" placeholder="First Name" class="form-control" />
                          </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-lastname">Last Name</label>
            <div class="col-sm-10">
              <input type="text" name="lastname" value="" placeholder="Last Name" id="input-lastname" class="form-control" />
                          </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email">E-Mail</label>
            <div class="col-sm-10">
              <input type="email" name="email" value="" placeholder="E-Mail" id="input-email" class="form-control" />
                          </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-telephone">Telephone</label>
            <div class="col-sm-10">
              <input type="tel" name="telephone" value="" placeholder="Telephone" id="input-telephone" class="form-control" />
                          </div>
          </div>
                  </fieldset>

        <fieldset>
          <legend>Your Password</legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password">Password</label>
            <div class="col-sm-10">
              <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control" />
                          </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-confirm">Password Confirm</label>
            <div class="col-sm-10">
              <input type="password" name="confirm_password" value="" placeholder="Password Confirm" id="input-confirm" class="form-control" />
                          </div>
          </div>
        </fieldset>
                        <div class="buttons">
          <div class="pull-right">
           <!--  <input type="submit" value="" class="btn btn-primary" /> -->
            <button type="submit" class="button"> Continue </button>
          </div>
        </div>
              </form>
      </div></div></div>



<aside id="column-right" class="col-right col-xs-12  col-sm-3">
    <div class="block block-account">
  <div class="block-title">
  Account  </div>
 <div class="block-content">
  <ul>
    <li><a href="login.php" >Login</a> </li>
  <li><a href="register.php" >Register</a> </li>
  <li><a href="#forgot_password.php" >Forgotten Password</a></li>
  </ul>
</div>
</div>
    </aside>
</div>
</div>
</div>

<div class="container-fluid">
<div class="row">
<div class="cat-box">




    <div class="col-md-2 col-sm-4 no-padding">
    <div class="cat-img">
    	<img src="image/cache/catalog/Restaurant/cat-img1-404x300.jpg" alt="Sandwich">
        <div class="cat-desc ">
            <h2>Sandwich</h2>
            <a href="index37b4.html?route=product/category&amp;path=1001_1006_1028">View Category</a>
         </div>
    </div>
    </div>



    <div class="col-md-2 col-sm-4 no-padding">
    <div class="cat-img">
    	<img src="image/cache/catalog/Restaurant/cat-img2-404x300.jpg" alt="Rasgulla">
        <div class="cat-desc white-color">
            <h2>Rasgulla</h2>
            <a href="indexbd14.html?route=product/category&amp;path=1001_1011_1050">View Category</a>
         </div>
    </div>
    </div>



    <div class="col-md-2 col-sm-4 no-padding">
    <div class="cat-img">
    	<img src="image/cache/catalog/Restaurant/cat-img3-404x300.jpg" alt="Rasmalai">
        <div class="cat-desc ">
            <h2>Rasmalai</h2>
            <a href="indexfa7b.html?route=product/category&amp;path=1001_1011_1049">View Category</a>
         </div>
    </div>
    </div>



    <div class="col-md-2 col-sm-4 no-padding">
    <div class="cat-img">
    	<img src="image/cache/catalog/Restaurant/cat-img4-404x300.jpg" alt="Hakka Noodles">
        <div class="cat-desc white-color">
            <h2>Hakka Noodles</h2>
            <a href="indexc3fa.html?route=product/category&amp;path=1003_1019_1083">View Category</a>
         </div>
    </div>
    </div>



    <div class="col-md-2 col-sm-4 no-padding">
    <div class="cat-img">
    	<img src="image/cache/catalog/Restaurant/cat-img5-404x300.jpg" alt="Breakfast">
        <div class="cat-desc ">
            <h2>Breakfast</h2>
            <a href="indexa8f1.html?route=product/category&amp;path=1002_1012">View Category</a>
         </div>
    </div>
    </div>



    <div class="col-md-2 col-sm-4 no-padding">
    <div class="cat-img">
    	<img src="image/cache/catalog/Restaurant/cat-img6-404x300.jpg" alt="Paneer Tikka">
        <div class="cat-desc ">
            <h2>Paneer Tikka</h2>
            <a href="indexff6f.html?route=product/category&amp;path=1001_1007_1032">View Category</a>
         </div>
    </div>
    </div>


</div>
</div>
</div>

<?php

  require_once 'template/footer.php';
  require_once 'template/mobile_menu.php';

 ?>

</body>
</html>
