<?php

class Hash{
  public static function make($string, $salt = '') {
    return hash('sha256', $string . $salt);
  }

  public function create($string) {
    return password_hash($string, PASSWORD_BCRYPT, ['cost' => 12]);
  }

  public static function salt($length) {
    return mcrypt_create_iv($length);
  }

  public static function unique() {
    return self::make(uniqid());
  }
}

 ?>
