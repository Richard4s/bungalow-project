<?php

class User {
  private $_db,
          $_data,
          $_sessionName,
          $_cookieName,
          $_isLoggedIn;

  public function __construct($user = null) {
    $this->_db = DB::getInstance();

    $this->_sessionName = Config::get('session/session_name');
    $this->_cookieName = Config::get('remember/cookie_name');


    if(!$user) {
      if(Session::exists($this->_sessionName)) {
        $user = Session::get($this->_sessionName);

        if($this->find($user)) {
          $this->_isLoggedIn = true;
        } else{

        }

      }
    } else {
      $this->find($user);
    }
  }

  public function update($fields = [], $id = null) {

    if(!$id && $this->isLoggedIn()) {
      $id = $this->data()->id;
    }

    if(!$this->_db->update('customers', $id, $fields)) { //Reset the $id here
      throw new Exception('There was a problem updating');
    }
  }

  public function create($fields = []) {
    if(!$this->_db->insert('customers', $fields)) {
      throw new Exception('There was a problem creating an account');
    }
  }

  public function find($user = null) {
    if($user) {
      $field = (is_numeric($user)) ? 'Id' : 'Email';
      $data = $this->_db->get('customers', [$field, '=', $user]);

      if($data->count()) {
        $this->_data = $data->first();
        return true;
      }
    }
    return false;
  }

  public function login($email = null, $password = null, $remember = false) {


    if(!$email && !$password && $this->exists()) {
      Session::put($this->_sessionName, $this->data()->Id);
    } else{
        $user = $this->find($email);
    if($user) {
      if( password_verify($this->data()->Password, Hash::create($this->data()->Password))) {
        Session::put($this->_sessionName, $this->data()->Id);

        if($remember) {
          $hash = Hash::unique();
          $hashCheck = $this->_db->get('customers_session', ['Id', '=', $this->data()->id]);

          if(!$hashCheck->count()) {
            $this->_db->insert('customers_session', [
              'Id' =>  $this->data()->id,
              'Hash' => $hash
            ]);
          } else{
            $hash = $hashCheck->first()->hash;
          }

          Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));

        }

        return true;
      }
    }
  }
  return false;
}

// public function getin($email = null, $password = null) {
//
//   if(!$email)
//
// }

// public function hasPermission($key) {
//   //global $group;
//   $group = $this->_db->get('groups', ['id', '=', $this->data()->group]);
//   print_r($group->first());
// }

public function hasPermission() {
    $userId = $this->data()->AccessLevel;

    $permissions = DB::getInstance()->query("SELECT * FROM customers WHERE AccessLevel = {$userId}");

//    if(!$permissions->results()) {
//      echo 'Nothing found';
//    } else {
//      foreach ($permissions->results() as $level){
//        echo $level;
//      }
//        //echo $permissions->results();
//    }

    //echo $permissions->first();
    //echo gettype($permissions);
    //$access = (int)$permissions;


//    echo $permissions;
//  $group = $this->_db->get('access', ['id', '=', $this->data()->AccessLevel]);
  //print_r($group->first());
  //return $group;

//  if($group->count()) {
//    $permissions = $group->first()->AccessLevel;
    //$permissions = json_decode($group->first()->permissions, true);
    //print_r($permissions);

      if($this->data()->AccessLevel == 2) {
        return true;
      }

//    if($permissions[$key] == true) {
//      return true;
//    }
    //return $permissions;
//  }
  return false;
}

  public function exists() {
    return (!empty($this->_data)) ? true : false;
  }

  public function logout() {

    $this->_db->delete('users_session', ['user_id', '=', $this->data()->id]);

    Session::delete($this->_sessionName);
    Cookie::delete($this->_cookieName);
  }

  public function data() {
    return $this->_data;
  }

  public function isLoggedIn() {
    return $this->_isLoggedIn;
  }
}

 ?>
