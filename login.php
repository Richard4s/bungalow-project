<?php

require_once 'core/init.php';
require_once 'template/header.php';

if(Input::exists()) {
  $validate = new Validate();
  $validation = $validate->check($_POST, [
    'email' => [
      'required' => true
    ],
    'password' => [
      'required' => true
    ]
  ]);

  if($validation->passed()) {
    $user = new User();

    $remember = (Input::get('remember') === 'on') ? true : false;
    $login = $user = $user->login(Input::get('email'), Input::get('password'), $remember);

    if($login) {
      echo 'Logged in.';

      $guy = new User();

        if($guy->hasPermission()) {
            echo '<script>window.location= "dashboard.php"</script>.';
        } else if(!$guy->hasPermission()) {
            echo '<script>window.location= "index.php"</script>.';
        }else {
            echo 'Something went wrong.';
        }

    } else{
      echo 'Sorry, Logging in failed.';
    }

  } else{
    foreach($validation->errors() as $error) {
      echo 'Something went wrong.'. $error;
    }
  }
}
require_once "vendor/autoload.php";

 ?>

 <style>

  .content{
    padding-left: 15px;
  }

 </style>

<div class="offer-box">
  <div class="container">
    <div class="row">
      <div class="col-md-10"><span> enjoy our food delivery</span></div>
    </div>
  </div>
</div>
<div id="mgkquickview">
<div id="magikloading" style="display:none;text-align:center;margin-top:400px;"><img src="catalog/view/theme/kabriodemo2/image/loading.gif" alt="loading">
</div></div><div class="main-container col2-right-layout">
<div class="main container">
<div class="account-login">
      <div class="row">                <div id="content" class="col-sm-12">      <div class="page-title"><h2>Account Login</h2></div>
      <div class="col2-set">
        <div class="col-1 new-users">
          <div class="content">
            <h2>New Customer</h2>
            <p><strong>Register Account</strong></p>
            <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
            <!-- <a href="" class="btn btn-primary"></a> -->
            <button onclick="window.location='register.php';" class="button create-account" type="button"><span>Continue</span></button>
          </div>
        </div>
        <div class="col-2 registered-users">
          <div class="content">
            <h2>Returning Customer</h2>
            <p><strong>I am a returning customer</strong></p>
            <form action="" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label" for="input-email">E-Mail Address</label>
                <input type="text" name="email" value="" placeholder="E-Mail Address" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password">Password</label>
                <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control" />
              </div>
             <!--  <input type="submit" value="" class="btn btn-primary" /> -->
              <button type="submit" class="button login"> Login </button>
              <a href="#forgot_password.php" class="forgot_pwd">Forgot Password</a>

                <p><br>Or Sign in with:</p>
                <?php



                //use  Firebase\JWT\JWT as JWT;


                $g_client = new Google_Client();
                $g_client->setClientId("8466497082-19k0j0uuu7l9l25ve33ev1jdcp0pjra0.apps.googleusercontent.com");
                $g_client->setClientSecret("McxvBQ9sJ8OsQFlL7fXQv3MK");
                $g_client->setRedirectUri("http://localhost/bungalow-2/google_signin.php");
                $g_client->setScopes("email");
                //Step 2 : Create the url
                $auth_url = $g_client->createAuthUrl();
                echo '<p><a href="'.$auth_url.'" class="button">Login with <img width="20" src="image/google-g.png" /> </a>';
                //Step 3 : Get the authorization  code
                $code = isset($_GET['code']) ? $_GET['code'] : NULL;


                //var_dump($code);
                if(isset($code)) {
                    try {
                        $token = $g_client->fetchAccessTokenWithAuthCode($code);
                        $g_client->setAccessToken($token);
                        //JWT::$leeway = 480;
                    }catch (Exception $e){
                        echo $e->getMessage();
                    }
                    try {
                        $pay_load = $g_client->verifyIdToken();
                        //var_dump($pay_load);
//                        var_dump($pay_load);
                    }catch (Exception $e) {
                        echo $e->getMessage();
                    }
                } else{
                    $pay_load = null;
                }

                echo $pay_load["email"];
                ?>



                <?php

                if(isset($_GET['state'])) {
                    $_SESSION['FBRLH_state'] = $_GET['state'];
                }


                $fb = new \Facebook\Facebook([
                    'app_id' => '155134925205387',
                    'app_secret' => '8356b39e8d242f9d4341b3a7438c9f13',
                    'default_graph_version' => 'v2.10',
                    //'default_access_token' => '{access-token}', // optional
                ]);

                // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
                //   $helper = $fb->getRedirectLoginHelper();
                //   $helper = $fb->getJavaScriptHelper();
                //   $helper = $fb->getCanvasHelper();
                //   $helper = $fb->getPageTabHelper();

                if(empty($access_token)) {
                    echo '<a class="button" href="'.$fb->getRedirectLoginHelper()->getLoginUrl("http://localhost/bungalow-2/facebook_login.php").'">Login with <img width="12" src="image/facebook-2.svg" /> </a></p>';
                }

                $access_token = $fb->getRedirectLoginHelper()->getAccessToken();

                if(isset($access_token)) {
                    try {
                        $response = $fb->get('/me',$access_token);
                        $fb_user = $response->getGraphUser();
                        echo  $fb_user->getName();
                        //  var_dump($fb_user);
                    } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                        echo  'Graph returned an error: ' . $e->getMessage();
                    } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                        // When validation fails or other local issues
                        echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    }
                }

                ?>
                          </form>
          </div>
        </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>


<?php



  require_once 'template/footer.php';
  require_once 'template/mobile_menu.php';

 ?>

</body>
</html>
