<?php


  require_once 'core/init.php';
  require_once 'template/header.php';

 ?>

<main id="page">



    <aside id="notifications">
        <div class="container">



        </div>
    </aside>


    <section id="wrapper">


        <div class="container">

            <div id="columns_inner">

                <div id="left-column" class="col-xs-12 col-sm-4 col-md-3 hb-animate-element top-to-bottom">

                    <div id="tm_vertical_menu_top" class="tmvm-contener clearfix col-lg-12  hb-animate-element top-to-bottom">
                        <div class="block-title">
                            <i class="material-icons menu-open">&#xE5D2;</i>
                            <div class="menu-title">Categories</div>

                        </div>



                        <div class="menu vertical-menu js-top-menu position-static hidden-sm-down" id="_desktop_top_menu">
                            <ul class="tm_sf-menu top-menu" id="top-menu" data-depth="0">

                                <li class="category " id="tmcategory-12"><a href="indexe734.html?id_category=12&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Bungalow Appetizer</a></li>
                                <li class="category " id="tmcategory-13"><a href="indexd9ce.html?id_category=13&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Bungalow Salads</a></li>
                                <li class="category " id="tmcategory-14"><a href="indexf052.html?id_category=14&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Sandwiches</a></li>
                                <li class="category " id="tmcategory-15"><a href="indexb34f.html?id_category=15&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Burgers & Hotdogs</a></li>
                                <li class="category " id="tmcategory-12"><a href="indexe734.html?id_category=12&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Pizza</a></li>
                                <li class="category " id="tmcategory-13"><a href="indexd9ce.html?id_category=13&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Pasta</a></li>
                                <li class="category " id="tmcategory-14"><a href="indexf052.html?id_category=14&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Beef & Chicken Platters</a></li>
                                <li class="category " id="tmcategory-15"><a href="indexb34f.html?id_category=15&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Char-Grilled Steaks</a></li>
                                <li class="category " id="tmcategory-20"><a href="index9378.html?id_category=20&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Nigerian Menu</a></li>
                                <li class="category " id="tmcategory-21"><a href="indexd081.html?id_category=21&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Burgers & Hotdogs</a></li>
                                <li class="category " id="tmcategory-22"><a href="index96da.html?id_category=22&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Seafood</a></li>
                                <li class="category " id="tmcategory-23"><a href="index8447.html?id_category=23&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Lite Section</a></li>
                                <li class="category " id="tmcategory-25"><a href="index82f7.html?id_category=25&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Salted Crepes </a></li>
                                <li class="category " id="tmcategory-24"><a href="indexbe45.html?id_category=24&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Sweet Crepes</a></li>

                                <li class="category " id="tmcategory-24"><a href="indexbe45.html?id_category=24&amp;controller=category&amp;id_lang=1" class="dropdown-item" data-depth="0">Drinks</a></li>
                            </ul>
                        </div>

                    </div>


                </div>


                <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">


                    <section id="main">

                        <section id="products">

                            <div id="">

                                <div id="js-product-list-top" class="row products-selection">

                                    <div class="col-md-6 hidden-md-down total-products">

                                        <ul class="display hidden-xs grid_list">

                                            <li id="grid"><a href="#" title="Grid">Grid</a></li>
                                            <li id="list"><a href="#" title="List">List</a></li>
                                        </ul>


                                    </div>

                                    <div class="col-sm-12 hidden-lg-up showing">
                                        Showing 1-2 of 2 item(s)
                                    </div>
                                </div>

                            </div>

                            <div id="">

                                <div id="js-product-list">
                                    <div id="spe_res">
                                        <div class="products">
                                            <ul class="product_list grid gridcount"> <!-- removed product_grid-->

                                                <li class="product_item col-xs-12 col-sm-6 col-md-6 col-lg-3">

                                                    <div class="product-miniature js-product-miniature" data-id-product="20" data-id-product-attribute="276" itemscope>
                                                        <div class="thumbnail-container">

                                                            <a href="index632b.html?id_product=20&amp;id_product_attribute=276&amp;rewrite=dishacentrupr-jenghing&amp;controller=product&amp;id_lang=1#/2-size-m/15-color-green/19-shoe_size-36" class="thumbnail product-thumbnail">
                                                                <img src = "image/resize/bangalow%20%201414.jpeg" alt = "" data-full-size-image-url = "image/resize/bangalow%20%201414.jpeg">

                                                                <img class="replace-2x img_1 img-responsive" src="image/resize/bangalow%20%201414.jpeg" data-full-size-image-url="image/resize/bangalow%20%201414.jpeg" alt="" />

                                                            </a>


                                                            <a href="#" class="quick-view" data-link-action="quickview">
                                                                <i class="material-icons search">&#xE8FF;</i><!-- Quick view -->

                                                            </a>
                                                        </div>

                                                        <div class="product-description">

                                                            <div class="product-price-and-shipping">
                                                                <h3 class="h3 product-title" itemprop="name"><a href="#">Pizza</a></h3>
                                                                <span itemprop="price" class="price">N6942</span>
                                                            </div>

                                                            <div class="product-actions">
                                                                <a href="#" class="btn btn-primary add-to-cart">
                                                                    Add To Cart
                                                                </a>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>


                                                <li class="product_item col-xs-12 col-sm-6 col-md-6 col-lg-3">

                                                    <div class="product-miniature js-product-miniature" data-id-product="20" data-id-product-attribute="276" itemscope>
                                                        <div class="thumbnail-container">

                                                            <a href="index632b.html?id_product=20&amp;id_product_attribute=276&amp;rewrite=dishacentrupr-jenghing&amp;controller=product&amp;id_lang=1#/2-size-m/15-color-green/19-shoe_size-36" class="thumbnail product-thumbnail">
                                                                <img src = "image/resize/bangalow%20%201418.jpeg" alt = "" data-full-size-image-url = "image/resize/bangalow%20%201418.jpeg">

                                                                <img class="replace-2x img_1 img-responsive" src="image/resize/bangalow%20%201418.jpeg" data-full-size-image-url="image/resize/bangalow%20%201418.jpeg" alt="" />

                                                            </a>


                                                            <a href="#" class="quick-view" data-link-action="quickview">
                                                                <i class="material-icons search">&#xE8FF;</i><!-- Quick view -->

                                                            </a>
                                                        </div>

                                                        <div class="product-description">
                                                            <h3 class="h3 product-title" itemprop="name"><a href="#">Bungalow Salad</a></h3>
                                                            <div class="product-price-and-shipping">
                                                                <span itemprop="price" class="price">N6942</span>
                                                            </div>

                                                            <div class="product-actions">
                                                                <a href="#" class="btn btn-primary add-to-cart">
                                                                    Add To Cart
                                                                </a>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>

                                                <li class="product_item col-xs-12 col-sm-6 col-md-6 col-lg-3">

                                                    <div class="product-miniature js-product-miniature" data-id-product="20" data-id-product-attribute="276" itemscope>
                                                        <div class="thumbnail-container">

                                                            <a href="index632b.html?id_product=20&amp;id_product_attribute=276&amp;rewrite=dishacentrupr-jenghing&amp;controller=product&amp;id_lang=1#/2-size-m/15-color-green/19-shoe_size-36" class="thumbnail product-thumbnail">
                                                                <img src = "image/resize/bangalow%20%201418.jpeg" alt = "" data-full-size-image-url = "image/resize/bangalow%20%201418.jpeg">

                                                                <img class="replace-2x img_1 img-responsive" src="image/resize/bangalow%20%201418.jpeg" data-full-size-image-url="image/resize/bangalow%20%201418.jpeg" alt="" />

                                                            </a>


                                                            <a href="#" class="quick-view" data-link-action="quickview">
                                                                <i class="material-icons search">&#xE8FF;</i><!-- Quick view -->

                                                            </a>
                                                        </div>

                                                        <div class="product-description">
                                                            <h3 class="h3 product-title" itemprop="name"><a href="#">Bungalow Salad</a></h3>
                                                            <div class="product-price-and-shipping">
                                                                <span itemprop="price" class="price">N6942</span>
                                                            </div>

                                                            <div class="product-actions">
                                                                <a href="#" class="btn btn-primary add-to-cart">
                                                                    Add To Cart
                                                                </a>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>

                                                <li class="product_item col-xs-12 col-sm-6 col-md-6 col-lg-3">

                                                    <div class="product-miniature js-product-miniature" data-id-product="20" data-id-product-attribute="276" itemscope>
                                                        <div class="thumbnail-container">

                                                            <a href="index632b.html?id_product=20&amp;id_product_attribute=276&amp;rewrite=dishacentrupr-jenghing&amp;controller=product&amp;id_lang=1#/2-size-m/15-color-green/19-shoe_size-36" class="thumbnail product-thumbnail">
                                                                <img src = "image/resize/bangalow%20%201418.jpeg" alt = "" data-full-size-image-url = "image/resize/bangalow%20%201418.jpeg">

                                                                <img class="replace-2x img_1 img-responsive" src="image/resize/bangalow%20%201418.jpeg" data-full-size-image-url="image/resize/bangalow%20%201418.jpeg" alt="" />

                                                            </a>


                                                            <a href="#" class="quick-view" data-link-action="quickview">
                                                                <i class="material-icons search">&#xE8FF;</i><!-- Quick view -->

                                                            </a>
                                                        </div>

                                                        <div class="product-description">
                                                            <h3 class="h3 product-title" itemprop="name"><a href="#">Bungalow Salad</a></h3>
                                                            <div class="product-price-and-shipping">
                                                                <span itemprop="price" class="price">N6942</span>
                                                            </div>

                                                            <div class="product-actions">
                                                                <a href="#" class="btn btn-primary add-to-cart">
                                                                    Add To Cart
                                                                </a>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>


                                    <nav class="pagination row">
                                        <div class="col-md-4">

                                            Showing 1-2 of 2 item(s)

                                        </div>

                                    </nav>


                                    <!--<div class="hidden-md-up text-xs-right up">
                                        <a href="#header" class="btn btn-secondary">
                                            Back to top
                                            <i class="material-icons">&#xE316;</i>
                                        </a>
                                    </div>-->
                                </div>

                            </div>

                        </section>

                    </section>


                </div>



            </div>
        </div>
    </section>
</main>


<?php


require_once 'template/footer.php';
require_once 'template/mobile_menu.php';
require_once 'template/for-shop.php';

?>

