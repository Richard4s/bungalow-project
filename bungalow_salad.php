<?php

require_once 'core/init.php';
require_once 'template/header.php';

//Session::delete('food');

//var_dump($_SESSION);

?>

    <div id="mgkquickview">
        <div id="magikloading" style="display:none;text-align:center;margin-top:400px;"><img src="catalog/view/theme/kabriodemo2/image/loading.gif" alt="loading">
        </div></div>
    <div class="breadcrumbs">
        <div class="container">

        </div>
    </div>
    <section class="main-container col2-left-layout">
        <div class="main container">
            <div class="row">
                <div id="content" class="col-sm-9 col-sm-push-3">
                    <div class="catalog-product-info">
                        <h2 class="page-heading"> <span class="page-heading-title">Bungalow Salads</span> </h2>
                        <div class="display-product-option">
                            <div class="toolbar top-toolbar">
                                <div class="sorter">
                                    <div class="view-mode">
                                        <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                                        <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                                    </div>
                                </div>
                            </div><!-- toolbar -->
                        </div>
                    </div>
                    <article class="col-main">
                        <div class="category-products">
                            <div class="pro_row products-list">

                                <?php
                                $retrieval = DB::getInstance()->query("SELECT * FROM bungalow_salads ORDER BY id DESC LIMIT 12");
                                if(!$retrieval->results()) {
                                    echo 'No products are available here yet. You can contact us to enquire <a href="contact_us.php">here.</a>';
                                }  else {
                                    foreach($retrieval->results() as $retrieves)  {
                                        echo '<div class="product-layout product-list">
                                            <div class="product-thumb col-item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info">
                                                            <a class="product-image" href="#" title="'.$retrieves->product_name.'">
                                                                <img src="uploads/'.$retrieves->product_image.'" alt="'.$retrieves->product_name.'" title="'.$retrieves->product_name.'"/>
                                                            </a>
                                                            <div class="sale-label sale-top-right">Sale</div>
                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title">
                                                                <a class="food_name" table-id="'.$retrieves->id.'" table-name="bungalow_salads" title="'.$retrieves->product_name.'" href="#">'.$retrieves->product_name.'</a>
                                                            </div>
                                                            <div class="desc std">
                                                                <p>'.$retrieves->product_description.'</p>
                                                            </div>
                                                            <div class="item-content">
        
                                                                <div class="item-price">
                                                                    <div class="price-box">
                                                                        <p class="special-price"><span class="price">N'.$retrieves->product_price.'</span></p>
                                                                    </div>
                                                                </div>
                                                                <div class="action">
                                                                    <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart the-clicked link-cart">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add to Cart</span>
                                                                    </button>
                                                                </div>
        
                                                            </div>
        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
                                    }

                                }

                                ?>

                            </div>
                        </div>
                    </article>

                </div>
                <aside id="column-left" class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
                    <div class="side-nav-categories">
                        <div class="block-title">Categories</div>
                        <div class="box-content box-category">

                            <ul>
                                <li class="">
                                    <a href="shop.php">Bungalow Appetizers</a>
                                </li>
                                <li class="">
                                    <a href="bungalow_salad.php" class="active">Bungalow Salad</a>
                                </li>
                                <li class="">
                                    <a href="burger_hotdog.php">Burgers & Hotdogs</a>
                                </li>
                                <li class="">
                                    <a href="char_grilled.php">Char-Grilled Steaks</a>
                                </li>
                                <li class="">
                                    <a href="drinks.php">Drinks</a>
                                </li>
                                <li class="">
                                    <a href="lite_section.php">Lite Section</a>
                                </li>
                                <li class="">
                                    <a href="nigerian_menu.php">Nigerian Menu</a>
                                </li>
                                <li class="">
                                    <a href="pasta.php">Pasta</a>
                                </li>
                                <li class="">
                                    <a href="pizza.php">Pizza</a>
                                </li>
                                <li class="">
                                    <a href="seafood.php">Seafood</a>
                                </li>
                                <li class="">
                                    <a href="sweet_crepes.php">Sweet Crepes</a>
                                </li>
                                <li class="">
                                    <a href="salted_crepes.php">Salted Crepes</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel panel-default special-products">
                        <div class="panel-heading">Surprise Me Packages</div>


                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="indexa9bd.html?route=product/product&amp;product_id=1002"><img src="image/cache/catalog/Restaurant/product2-700x850.jpg" alt="Aloo kathi roll" title="Aloo kathi roll" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="indexa9bd.html?route=product/product&amp;product_id=1002">Aloo kathi roll</a></h4>


                                    <p class="price">
                                        <span class="price-new">$200.00</span> <span class="price-old">$279.99</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="indexb3b3.html?route=product/product&amp;product_id=1015"><img src="image/cache/catalog/Restaurant/product15-700x850.jpg" alt="Glass Noodle Salad" title="Glass Noodle Salad" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="indexb3b3.html?route=product/product&amp;product_id=1015">Glass Noodle Salad</a></h4>


                                    <p class="price">
                                        <span class="price-new">$190.00</span> <span class="price-old">$200.00</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="indexfe6d.html?route=product/product&amp;product_id=1006"><img src="image/cache/catalog/Restaurant/product6-700x850.jpg" alt="Grilled Swordfish" title="Grilled Swordfish" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="indexfe6d.html?route=product/product&amp;product_id=1006">Grilled Swordfish</a></h4>


                                    <p class="price">
                                        <span class="price-new">$180.00</span> <span class="price-old">$200.00</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="index2998.html?route=product/product&amp;product_id=1017"><img src="image/cache/catalog/Restaurant/product17-700x850.jpg" alt="Plato de comida elegante" title="Plato de comida elegante" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="index2998.html?route=product/product&amp;product_id=1017">Plato de comida elegante</a></h4>


                                    <p class="price">
                                        <span class="price-new">$90.00</span> <span class="price-old">$100.00</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>

<?php

require_once 'template/footer.php';
require_once 'template/mobile_menu.php';
require_once 'template/scripts.php';

?>