<?php
ob_start();
require_once 'core/init.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

$noproduct = false;

if(!isset($_GET['id'])){
    $noproduct = true;
}else{
    $id = intval($_GET['id']);
}

$stmt = $conn->stmt_init();
$error= false;$message = array();$success = false;

if(isset($_POST['delete_item_submit'])){
    $sql = 'DELETE FROM featured WHERE ProductId = ?';
    $stmt->prepare($sql);
    $stmt->bind_param('i',$id);
    $stmt->execute();
    if($stmt->affected_rows > 0){
        $success = true;
        header("Location:dashboard_featured.php");
        exit;
    }else{
        $error = true;
        $message[] = 'Couldnt unmark product';
    }
    $stmt->free_result();
}


?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Unmark Product</h2>
                <ul>
                <?php 
                    if($_POST && $error){
                        foreach($message as $messages){ ?>
                            <li><?= $messages; ?></li>
                        <?php }
                    }elseif($_POST && $success){
                        foreach($message as $messages){ ?>
                            <li><?= $messages; ?></li>
                        <?php }
                    }
                ?>
                </ul>
            </div>
        </div>
        <!-- /.panel-cover -->

        <?php if($noproduct){
            echo "<h3 style=\"padding:2%;\">Cannot find product!</h3>";
        }else{?>
        <div class="panel-body">
            <form class="mdform" method="post" action="" enctype="multipart/form-data">
                
                <h2>Are you sure you want to unmark this product?</h2>

                <button name="delete_item_submit" id="ajax-submit" type="submit" class="btn btn-danger">Confirm</button>
            </form>
        </div>
        <?php } ?>

        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->



</div>

<?php 
ob_end_flush();
?>