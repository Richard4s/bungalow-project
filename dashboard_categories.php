<?php

require_once 'core/init.php';
require_once 'classes/Upload.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

$stmt = $conn->stmt_init();

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Manage Categories</h2>
                
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <table border="0" class="table">
                <tr>
                    <th>Category</th>
                    <th>&nbsp;</th>
                </tr>
                <?php 
                $sql = "SELECT b.Id, b.CategoryName, c.ProductCategoryName
                FROM category b, subproductcategory c
                WHERE b.SubProductCategoryId = c.Id";
                $stmt->prepare($sql);
                $stmt->bind_result($id, $catname, $bigcatname);
                $stmt->execute();  
                $stmt->store_result();
                if(!$stmt->num_rows > 0){
                    echo 'No Categories available';
                }else{ 
                
                    while($stmt->fetch()){ ?>
                        <tr>
                            <td><?= ucfirst($catname).'('.$bigcatname.')'; ?></td>
                            <td><a href="dashboard_category_delete.php?id=<?= $id; ?>" class="btn btn-danger">Delete</a></td>
                        </tr>
                <?php   }
                    
                }
                ?>
            </table>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>
