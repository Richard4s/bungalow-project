<?php

require_once 'core/init.php';

$user = new User();

if(!$user->isLoggedIn()) {
    if(!$user->hasPermission('admin')) {
        echo '<script>window.location="include/errors/404.php"</script>';
    }
}

if(isset($_GET['id']) && isset($_GET['category'])) {

    try {

        $deleteid = $_GET['id'];
        $category = $_GET['category'];
        $deleteProduct = DB::getInstance()->query("DELETE FROM {$category} WHERE id={$deleteid}");
        echo 'Successful';
        echo '<script>window.location="dashboard.php"</script>';

    } catch(Exception $e) {
        echo 'Something went wrong. Error: '.$e;
    }


}