<?php

    require_once 'dashboard_header.php';
    require_once 'dashboard_sidebar.php';

?>

    <div class="page">

        <header class="ribbon">
            <h2>
                Dashboard
            </h2>
            <ol class="breadcrumb">
                 <li class=""><a href="#">Home</a></li>
                 <li class="active">Dashboard</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-raised" >
                        <div class="">
                            <div class="panel-heading no-padding panel-item-raised pull-left">
                                <div class="square-icon square-icon-lg square-icon-primary">
                                    <i class="fa fa-gamepad"></i>
                                </div>
                                <!-- /.media-block -->

                            </div>
                        </div>

                        <div class="panel-body ">
                            <div class="align-right">
                                <h6>Total Online Payments</h6>
                                <h2>
                                    1,200,000
                                </h2>
                            </div>
                            <div class="chart-container">
                                <div class="widget-chart">
                                    <canvas id="chart-line" width="400" height="87"></canvas>

                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-md-4 -->
                <div class="col-lg-4">


                    <div class="panel panel-raised" style="">
                        <div class="">
                            <div class="panel-heading no-padding panel-item-raised pull-left">
                                <div class="square-icon square-icon-lg square-icon-info">
                                    <i class="fa fa-retweet"></i>
                                </div>
                                <!-- /.media-block -->

                            </div>
                        </div>

                        <div class="panel-body ">
                            <div class="align-right">
                                <h6>Total Payment On Delivery</h6>
                                <h2>
                                    3,240,100
                                </h2>
                            </div>
                            <div class="chart-container">
                                <div class="widget-chart">
                                    <canvas id="chart-line2" width="400" height="87"></canvas>

                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>



                </div>

                <div class="col-lg-4">
                    <div class="panel panel-raised" style="">
                        <div class="">
                            <div class="panel-heading no-padding panel-item-raised pull-left">
                                <div class="square-icon square-icon-lg square-icon-success">
                                    <i class="fa fa-dollar"></i>
                                </div>
                                <!-- /.media-block -->

                            </div>
                        </div>

                        <div class="panel-body ">
                            <div class="align-right">
                                <h6>Total Sales</h6>
                                <h2>
                                    11,240,100
                                </h2>
                            </div>
                            <div class="chart-container">
                                <div class="widget-chart">
                                    <canvas id="chart-line3" width="400" height="87"></canvas>


                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>



                </div>
            </div>
            <!-- /.row -->
            <div class="row">

                <!-- /.col-lg-4 -->
                <div class="col-lg-8">

                    <div class="panel">
                        <div class="panel-heading">
                          <h2>
                              Monthly Sale
                          </h2>
                          <span class="subtext">
                              From July to November 2017
                          </span>
                            <ul class="panel-actions">
                                <li>
                                    <a href="#" class="panel-action">
                                        <i class="mdi mdi-dots-vertical">

                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="panel-action panel-collapse-toggle">
                                        <i class="mdi mdi-chevron-up">

                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="panel-action panel-action-fullscreen ">
                                        <i class="mdi mdi-fullscreen"></i>
                                    </a>
                                </li>

                            </ul>
                        </div>

                        <div class="panel-body" style="padding-top:0.5em;">

                            <div class="chart-legends"  style="text-align:right; padding-right:1em;">
                                <span>
                                    <i class="fa fa-circle mg-sm" style="color:rgba(36,169,188,1);"></i>Product Sale

                                    <i class="fa fa-circle mg-sm" style="color:rgba(30,126,239,1);"></i>Visitors
                                    <i class="fa fa-circle mg-sm" style="color:rgba(115,89,238,1);"></i>Revenue
                                </span>
                            </div>

                            <div class="chart-container" style="position:relative;">

                                <canvas id="chart-bar" width="500" height="300"></canvas>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel">
                        <div class="panel-heading">
                          <h2>
                              Monthly Sale
                          </h2>
                          <span class="subtext">
                              From July to November 2017
                          </span>
                          <ul class="panel-actions">

                              <li>
                                  <a href="#" class="panel-action panel-collapse-toggle">
                                      <i class="mdi mdi-chevron-up">

                                      </i>
                                  </a>
                              </li>


                          </ul>
                        </div>
                        <div class="panel-body">
                            <div id="donut-chart"></div>


                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                </div>
                <!-- /.col-md-4 -->

            </div>
            <!-- /.row -->

              <!-- /.row -->

        </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
