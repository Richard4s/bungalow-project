<div class="sidebar sidebar-left sidebar-dark sidebar-fixed sidebar-navbar-theme" id="sidebar">


    <div class="sidebar-scrollable-content">


        <div class="sidebar-body">


            <div class="sidebar-cover">
                <a class="sidebar-user" data-toggle="collapse" href="#sidebar-highlight" aria-expanded="false" aria-controls="sidebar-highlight">

                    <div class="sidebar-user-img">
                        <img src="image/catalog/Restaurant/logo_demo2.png" alt="" class="img-circle img-online img-thumbnailimg-thumbnail-primary">
                    </div>
                    <div class="sidebar-user-name">
                        <?php $user = new User();
                        echo escape($user->data()->FirstName); ?>
                        <span class="sidebar-user-expand"><i class="fa fa-caret-down"></i></span>
                        <span class="text-small sidebar-user-email">
                    <?php echo escape($user->data()->Email); ?>
                </span>
                    </div>

                </a>
                <div class="sidebar-highlight collapse" id="sidebar-highlight">

                    <ul class="main-nav">
                        <!-- <li>
                            <a href="#"><i class="mdi mdi-settings"></i> Manage Users</a>
                        </li> -->
                        <!-- <li>
                            <a href="#"><i class="mdi mdi-account"></i> Profile</a>
                        </li> -->
                        <li>
                            <a href="logout.php"><i class="mdi mdi-logout"></i> Logout</a>
                        </li>
                    </ul>

                </div>
            </div>


            <div class="main-menu-container">
                <ul class="main-nav" id="main-nav">
                    <li class="main-nav-label">
                <span>
                    Main Navigation
                </span>
                    </li>
                    <li>
                        <a href="dashboard.php">

                            <i class="mdi mdi-cube"></i><span class="title">Dashboard</span>


                        </a>
                    </li>


                    <li>
                        <a href="dashboard_add.php" class="has-arrow">
                            <i class="mdi mdi-plus"></i><span class="title">Add Products</span>
                        </a>
                    </li>



                    <li>
                        <a href="dashboard_products.php" class="has-arrow">
                            <i class="mdi mdi-lead-pencil"></i><span class="title">Manage Products</span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="dashboard_add_category.php" class="has-arrow">
                            <i class="mdi mdi-plus"></i><span class="title">Add Category</span>
                        </a>
                    </li>

                    <li>
                        <a href="dashboard_categories.php" class="has-arrow">
                            <i class="mdi mdi-lead-pencil"></i><span class="title">Manage Categories</span>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_add_featured.php" class="has-arrow">
                            <i class="mdi mdi-cup"></i><span class="title">Add Featured Products</span>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_featured.php" class="has-arrow">
                            <i class="mdi mdi-cup"></i><span class="title">Manage Featured Products</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="has-arrow">
                            <i class="mdi mdi-email-open"></i><span class="title">Send Newsletters</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="has-arrow">
                            <i class="mdi mdi-cellphone-basic"></i><span class="title">Send Sms</span>
                        </a>
                    </li>


                </ul>
            </div>


            <!-- /.main-menu-container -->
        </div>
        <!-- /.sidebar-body -->
    </div>
    <!-- /.sidebar-scrollable-content -->

    <div class="sidebar-footer">
        <div class="horizontal-nav">
            <ul class="horizontal-nav horizontal-nav-3">
                <li>
                    <a href="#"><i class="mdi mdi-settings"></i></a>
                </li>
                <li>
                    <a href="#"><i class="mdi mdi-account"></i></a>
                </li>
                <li>
                    <a href="logout.php"><i class="mdi mdi-logout"></i></a>
                </li>
            </ul>
        </div>
    </div>


</div>