<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="catalog/view/javascript/bootstrap/js/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="js/bootstrap-notify.min.js"></script>
<?php

require_once 'core/init.php';
require_once 'template/header.php';

if(isset($_GET['id'])) {
    $category_id = $_GET['id'];
} else{
    $category_id = 1;
}

?>

    <div id="mgkquickview">
        <div id="magikloading" style="display:none;text-align:center;margin-top:400px;"><img src="catalog/view/theme/kabriodemo2/image/loading.gif" alt="loading">
        </div></div>
    <div class="breadcrumbs">
        <div class="container">

        </div>
    </div>
    <section class="main-container col2-left-layout">
        <div class="main container">
            <div class="row">
                <div id="content" class="col-sm-9 col-sm-push-3">
                    <div class="catalog-product-info">
                        <h2 class="page-heading"> <span class="page-heading-title">Bungalow Appetizer</span> </h2>

                    </div>
                    <article class="col-main">
                        <div class="category-products">
                            <div class="pro_row products-list">

                                <?php
                                $retrieval = DB::getInstance()->query("SELECT * FROM bungalow_appetizers ORDER BY id DESC LIMIT 12");
                                if(!$retrieval->results()) {
                                    echo 'No products are available here yet. You can contact us to enquire <a href="contact_us.php">here.</a>';
                                }  else {
                                    foreach($retrieval->results() as $retrieves)  {
                                        echo '<div class="product-layout product-list">
                                            <div class="product-thumb col-item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info">
                                                            <a class="product-image" href="#" title="'.$retrieves->product_name.'">
                                                                <img src="uploads/'.$retrieves->product_image.'" alt="'.$retrieves->product_name.'" title="'.$retrieves->product_name.'"/>
                                                            </a>
                                                            <div class="sale-label sale-top-right">Sale</div>
                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title">
                                                                <a table-name="bungalow_appetizers" table-id="'.$retrieves->id.'" class="food_name" title="'.$retrieves->product_name.'" href="#">'.$retrieves->product_name.'</a>
                                                            </div>
                                                            <div class="desc std">
                                                                <p>'.$retrieves->product_description.'</p>
                                                            </div>
                                                            <div class="item-content">
        
                                                                <div class="item-price">
                                                                    <div class="price-box">
                                                                        <p class="special-price"><span class="price">N'.$retrieves->product_price.'</span></p>
                                                                    </div>
                                                                </div>
                                                                <div class="action">
                                                                    <button type="button" title="" data-original-title="Add to Cart" class="button the-clicked btn-cart link-cart">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add to Cart</span>
                                                                    </button>
                                                                </div>
        
                                                            </div>
        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
                                    }

                                }

                                ?>

                            </div>
                        </div>
                    </article>

                </div>
                <aside id="column-left" class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
                    <div class="side-nav-categories">
                        <div class="block-title">Categories</div>
                        <div class="box-content box-category">

                            <ul>

                                <li class="">
                                    <a href="shop.php">Bungalow</a>
                                    <span class="subDropdown minus"></span>
                                    <ul class="level0" style="display: block;">
                                        <?php
                                        $retrieval = DB::getInstance()->query("SELECT * FROM category WHERE SubProductCategoryId = 1");
                                        if(!$retrieval->results()) {
                                            echo 'No products are available here yet. You can contact us to enquire <a href="contact_us.php">here.</a>';
                                        } else {
                                            foreach($retrieval->results() as $retrieves) {
                                                echo '<li><a href="#" class="">'.$retrieves->CategoryName.'</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="shop.php">Bunyaki</a>
                                    <span class="subDropdown minus"></span>
                                    <ul class="level0" style="display: block;">
                                        <?php
                                        $retrieval = DB::getInstance()->query("SELECT * FROM category WHERE SubProductCategoryId = 2");
                                        if(!$retrieval->results()) {
                                            echo 'No products are available here yet. You can contact us to enquire <a href="contact_us.php">here.</a>';
                                        } else {
                                            foreach($retrieval->results() as $retrieves) {
                                                echo '<li><a href="#" class="">'.$retrieves->CategoryName.'</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="shop.php">Habanero's</a>
                                    <span class="subDropdown minus"></span>
                                    <ul class="level0" style="display: block;">

                                        <?php
                                        $retrieval = DB::getInstance()->query("SELECT * FROM category WHERE SubProductCategoryId = 3");
                                        if(!$retrieval->results()) {
                                            echo 'No products are available here yet. You can contact us to enquire <a href="contact_us.php">here.</a>';
                                        } else {
                                            foreach($retrieval->results() as $retrieves) {
                                                echo '<li><a href="#" class="">'.$retrieves->CategoryName.'</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="">
                                    <a href="drinks.php">Drinks</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel panel-default special-products">
                        <div class="panel-heading">Surprise Me Packages</div>


                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="indexa9bd.html?route=product/product&amp;product_id=1002"><img src="image/cache/catalog/Restaurant/product2-700x850.jpg" alt="Aloo kathi roll" title="Aloo kathi roll" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="indexa9bd.html?route=product/product&amp;product_id=1002">Aloo kathi roll</a></h4>


                                    <p class="price">
                                        <span class="price-new">$200.00</span> <span class="price-old">$279.99</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="indexb3b3.html?route=product/product&amp;product_id=1015"><img src="image/cache/catalog/Restaurant/product15-700x850.jpg" alt="Glass Noodle Salad" title="Glass Noodle Salad" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="indexb3b3.html?route=product/product&amp;product_id=1015">Glass Noodle Salad</a></h4>


                                    <p class="price">
                                        <span class="price-new">$190.00</span> <span class="price-old">$200.00</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="indexfe6d.html?route=product/product&amp;product_id=1006"><img src="image/cache/catalog/Restaurant/product6-700x850.jpg" alt="Grilled Swordfish" title="Grilled Swordfish" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="indexfe6d.html?route=product/product&amp;product_id=1006">Grilled Swordfish</a></h4>


                                    <p class="price">
                                        <span class="price-new">$180.00</span> <span class="price-old">$200.00</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-thumb transition">
                                <div class="image"><a href="index2998.html?route=product/product&amp;product_id=1017"><img src="image/cache/catalog/Restaurant/product17-700x850.jpg" alt="Plato de comida elegante" title="Plato de comida elegante" class="img-responsive" /></a></div>
                                <div class="caption">
                                    <h4><a href="index2998.html?route=product/product&amp;product_id=1017">Plato de comida elegante</a></h4>


                                    <p class="price">
                                        <span class="price-new">$90.00</span> <span class="price-old">$100.00</span>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                     </aside>

                <div id="content" class="col-sm-3">
                    <p>Helllo aWorld</p>
                </div>
            </div>
        </div>
    </section>

<?php

require_once 'template/footer.php';
require_once 'template/mobile_menu.php';
require_once 'template/scripts.php';

?>