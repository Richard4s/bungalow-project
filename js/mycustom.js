

$(document).ready(function() {

   var editajax = function() {
       $('#ajax-submit').click(function() {
           var currentPage = $(location).attr('href');
           var category = $('#optionguys').val();
           $.post(currentPage, {selectCat: category});
       });
   };

    editajax();

  var userFeed = new Instafeed({
    get: 'user',
    userId: '2086153560',
    limit: 4,
    resolution: 'standard_resolution',
    accessToken: '2086153560.1677ed0.0d95bf9635cc4798bce44ef619ec6465',
    sortBy: 'most-recent',
    template: '<div class="col-md-3"><a href="{{image}}" title="{{caption}}" target="_blank"><img class="thumbnail img-responsive" src="{{image}}" alt="{{caption}}" class="img-fluid" /></a></div>'
  });
  userFeed.run();


  var trySession = function() {
      var count = 0;

      $('#theclicked').click(function() {
          alert('Successfully added an item to cart');
          var el = $('#food_name');
          // var food = el.text();
          var foodId = el.attr('table-id');
          var table_name = el.attr('table-name');
          getNumber(foodId);
          // alert(table_name);

          $.ajax({
              type: 'POST',
              url: 'cart.php',
              data: { foodId: foodId, tableName: table_name },
              success: function(response) {
                  $('.cart_count').text(count);
                  $('.cart_item').html(response + ' You have <b>' + count + '</b> items in the cart<br><a href="checkout.php" class="btn">Go to cart</a><button class="btn" onclick="clearance();">Clear Cart</button>')
                  // alert(response);
              },
              error: function(response) {
                  $('.cart_item').html(response);
              }
          });
          //$.post('cart.php', {food: food, id: foodId}, function(response) {
              //alert('Worked' + response);
              count++;

              // $('.cart_count').text(count);
              // $('.cart_item').html('You have <b>' + count + '</b> items in the cart<br><a href="checkout.php" class="btn">Go to cart</a><button class="btn" onclick="clearance();">Clear Cart</button>');
              //location.reload();
          //});
      });

  };

  function getNumber(value) {
      value = value.replace(/\D/g, '');
      value = parseInt(value, 10);
      return value;
  }

  trySession();

  // var clearance = function() {
  //   // $.post('cart.php', {food: food}, function(response) {
  //   //
  //   // });
  //     alert('hi');
  // };

    // function clearance() {
    //     alert('hi');
    // }


    var countdown = function(end, elements, callback){
        var _second = 1000,
            _minute = _second * 60,
            _hour = _minute * 60,
            _day = _hour * 24,

            end = new Date(end),
            timer,


            calculate = function(){

                var now = new Date(),
                    remaining = end.getTime() - now.getTime(),
                    data;

                if (isNaN(end)){
                    console.log('Invalid date/time');
                    return;
                }

                if (remaining <= 0){
                    clearInterval(timer);

                    if (typeof callback === 'function'){
                        callback();
                    }

                } else{
                    if (!timer){
                        timer = setInterval(calculate, _second);
                    }

                    data = {
                        'days': Math.floor(remaining / _day),
                        'hours': Math.floor((remaining % _day) / _hour),
                        'minutes': Math.floor((remaining % _hour) / _minute),
                        'seconds': Math.floor((remaining % _minute) / _second),
                    }

                    if (elements.length){
                        for (x in elements){
                            var x = elements[x];
                            data[x] = ('00' + data [x]).slice(-2);
                            document.getElementById(x).innerHTML = data[x];
                        }
                    }

                }


            };

        calculate();
    };

});
