<?php

require_once 'vendor/autoload.php';

session_start();
if(isset($_GET['state'])) {
    $_SESSION['FBRLH_state'] = $_GET['state'];
}


$fb = new \Facebook\Facebook([
    'app_id' => '152158492118383',
    'app_secret' => 'ba7f0bf9a189fd42c9675ec5bc767d61',
    'default_graph_version' => 'v2.10'
    //'default_access_token' => '{access-token}', // optional
]);

// Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
//   $helper = $fb->getRedirectLoginHelper();
//   $helper = $fb->getJavaScriptHelper();
//   $helper = $fb->getCanvasHelper();
//   $helper = $fb->getPageTabHelper();

if(empty($access_token)) {
    echo "<a href='{$fb->getRedirectLoginHelper()->getLoginUrl("http://localhost/bungalow-2/facebook_login.php")}'>Login with Facebook </a>";
}

$access_token = $fb->getRedirectLoginHelper()->getAccessToken();

if(isset($access_token)) {
    try {
        $response = $fb->get('/me',$access_token);
        $fb_user = $response->getGraphUser();
        echo  $fb_user->getName();
        //  var_dump($fb_user);
    } catch (\Facebook\Exceptions\FacebookResponseException $e) {
        echo  'Graph returned an error: ' . $e->getMessage();
    } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
    }
}

//$me = $response->getGraphUser();
//echo 'Logged in as ' . $me->getName();