<?php

require_once 'core/init.php';
require_once 'classes/Upload.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

$stmt = $conn->stmt_init();

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Manage Products</h2>
                
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <table border="0" class="table">
                <tr>
                    <th>Product Name</th>
                    <th>Category</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                <?php 
                $sql = "SELECT a.Id, a.ProductName, DATE_FORMAT(a.DateCreated, '%d %b %Y') as DateCreated, 
                b.CategoryName, c.ProductCategoryName
                FROM products a, category b, subproductcategory c
                WHERE a.ProductCategoryId = b.Id AND b.SubProductCategoryId = c.Id
                ORDER BY DateCreated DESC";
                $stmt->prepare($sql);
                $stmt->bind_result($id, $name, $date, $catname, $bigcatname);
                $stmt->execute();  
                $stmt->store_result();
                if($stmt->num_rows == 0){
                    echo 'No Products available';
                }elseif($stmt->num_rows > 0){ 
                
                    while($stmt->fetch()){ ?>
                        <tr>
                            <td><?= ucfirst($name); ?></td>
                            <td><?= ucfirst($catname).'('.$bigcatname.')'; ?></td>
                            <td><a href="dashboard_edit.php?id=<?= $id; ?>" class="btn btn-primary">Edit</a></td>
                            <td><a href="dashboard_delete.php?id=<?= $id; ?>" class="btn btn-danger">Delete</a></td>
                        </tr>
                <?php   }
                    
                }
                ?>
            </table>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>
