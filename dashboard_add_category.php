<?php

require_once 'core/init.php';
require_once 'classes/Upload.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

$stmt = $conn->stmt_init();

if(array_key_exists('add_item_submit', $_POST)){
    $name = $_POST['add_item_name'];

    $category = $_POST['optionguys'];

    $error = false; $message = array(); $success = false;

    if(empty($name)||$category==0){
        $error=true;
        $message[] = 'Fill up the fields';    
    }else{
        //if not errors, add to DB
            
        $sql = "INSERT INTO category (CategoryName,SubProductCategoryId)
        VALUES (?,?)";
        if($stmt->prepare($sql)){
            $stmt->bind_param('si', $name,$category);
            $stmt->execute();
            if($stmt->affected_rows > 0){
                $success = true;
                header("Location:dashboard_categories.php");
                exit;
            }else{
                $error = true;
                $message[] = 'Couldnt insert record into database';
            }
            
        }
        $stmt->free_result();
        
              
    }
}

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Add Product Category</h2>
                <ul>
                <?php 
                    if($_POST && $error){
                        foreach($message as $messages){ ?>
                            <li><?= $messages; ?></li>
                        <?php }
                    }
                ?>
                </ul>
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <form class="mdform" method="post" action="">
                <div class="form-group md-form-group">

                    <label for="form1-email">Name of Category</label>
                    <input name="add_item_name" type="text" class="form-control">
                </div>
                <div class="form-group md-form-group">
                    <h4>Select Section</h4>
                    <select name="optionguys">
                        <option value="0"><span class="label-text">Select One:</span></option>
                        <?php
                            $sql = "SELECT Id, ProductCategoryName FROM subproductcategory";
                            if($stmt->prepare($sql)){
                                $stmt->execute();
                                $stmt->bind_result($a, $b);
                                $stmt->store_result();
                                $numrows = $stmt->num_rows();
                                if($numrows > 0){
                                    while($stmt->fetch()){ ?>
                                <option value="<?= $a; ?>"><span class="label-text"><?= $b; ?></span></option>     
                                <?php }
                                }
                            }
                        ?>
                    </select>
                </div>


                </div>
                <button name="add_item_submit" type="submit" class="btn btn-info">Submit</button>

            </form>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>