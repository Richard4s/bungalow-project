<?php

require_once 'core/init.php';
require_once 'classes/Upload.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

$stmt = $conn->stmt_init();

if(array_key_exists('add_item_submit', $_POST)){
    $category = $_POST['optionguys'];

    $error = false; $message = array(); $success = false;

    if($category==0){
        $error=true;
        $message[] = 'Fill up the fields';    
    }else{
        //if not errors, add to DB
            
        $sql = "INSERT INTO featured (ProductId)
        VALUES (?)";
        if($stmt->prepare($sql)){
            $stmt->bind_param('i', $category);
            $stmt->execute();
            if($stmt->affected_rows > 0){
                $success = true;
                header("Location:dashboard_featured.php");
                exit;
            }else{
                $error = true;
                $message[] = 'Couldnt mark product as featured';
            }
            
        }
        $stmt->free_result();
        
              
    }
}

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Add Featured Product</h2>
                <ul>
                <?php 
                    if($_POST && $error){
                        foreach($message as $messages){ ?>
                            <li><?= $messages; ?></li>
                        <?php }
                    }
                ?>
                </ul>
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <form class="mdform" method="post" action="">
                <div class="form-group md-form-group">
                    <h4>Select Product</h4>
                    <select name="optionguys">
                        <option value="0"><span class="label-text">Select One:</span></option>
                        <?php
                            $sql = "SELECT a.Id, a.ProductName, b.CategoryName, c.ProductCategoryName 
                            FROM products a, category b, subproductcategory c
                            WHERE a.ProductCategoryId = b.Id AND b.SubProductCategoryId = c.Id
                            AND a.Id NOT IN (SELECT ProductId from featured)";
                            if($stmt->prepare($sql)){
                                $stmt->execute();
                                $stmt->bind_result($a, $b, $c, $d);
                                $stmt->store_result();
                                $numrows = $stmt->num_rows();
                                if($numrows > 0){
                                    while($stmt->fetch()){ ?>
                                <option value="<?= $a; ?>"><span class="label-text"><?= ucfirst($b).'- '.$c.' ('.$d.')'; ?></span></option>     
                                <?php }
                                }
                            }
                        ?>
                    </select>
                </div>


                </div>
                <button name="add_item_submit" type="submit" class="btn btn-info">Add</button>

            </form>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>