<?php

require_once 'core/init.php';
require_once 'template/header.php';

//Session::delete('');

//session_destroy();
?>


<style>
    .thicker-font {
        font-size: 20px !important;
    }
</style>
<div id="mgkquickview">
    <div id="magikloading" style="display:none;text-align:center;margin-top:400px;"><img src="catalog/view/theme/kabriodemo2/image/loading.gif" alt="loading">
    </div></div><div>
    <div id="content">

        <div id="magik-slideshow" class="magik-slideshow">
            <div class="container-fluid">
                <div class="row">
                    <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
                        <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                            <ul>

                                <!-- <script type="text/javascript">
                                ₦(document).ready(function(){
                                       ₦("#ms0").click(function() {
                                              window.open('http://www.magikcommerce.com/','_blank');
                                       });   });  </script> -->

                                <li id="ms0" data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='image/_026.jpeg'><img src='image/_026.jpeg' alt="Slider Image1" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />

                                    <div class="info">
                                        <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;max-width:auto;max-height:auto;white-space:nowrap;'><span>Good for you</span> </div>
                                        <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'><span>Organic Week</span> </div>
                                        <!-- <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>In augue urna, nunc, tincidunt, augue, augue facilisis facilisis.</div>   -->
                                        <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><a href='shop.php' class="buy-btn">Shop Now</a> </div>

                                    </div>
                                </li>

                                <!-- <script type="text/javascript">
                                ₦(document).ready(function(){
                                       ₦("#ms1").click(function() {
                                              window.open('http://www.magikcommerce.com/','_blank');
                                       });   });  </script> -->

                                <li id="ms1" data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='image/_021.jpeg'><img src='image/_021.jpeg' alt="Slider Image2" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />

                                    <div class="info">
                                        <div class='tp-caption ExtraLargeTitle sft slide2  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;max-width:auto;max-height:auto;white-space:nowrap;padding-right:0px'><span>Mega Sale</span> </div>
                                        <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'>Go Lightly</div>
                                        <!-- <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div> -->

                                        <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><a href='shop.php' class="buy-btn">Shop Now</a> </div>

                                    </div>
                                </li>
                            </ul>
                        </div><!-- rev_slider_4 -->
                    </div><!-- rev_slider_4_wrapper --><!-- rev_slider_4_wrapper -->
                </div>
            </div>
        </div>
        <script type='text/javascript'>
            jQuery(document).ready(function() {
                jQuery('#rev_slider_4').show().revolution({
                    dottedOverlay: 'none',
                    delay: 5000,
                    startwidth: 1920,
                    startheight: 800,
                    hideThumbs: 200,
                    thumbWidth: 200,
                    thumbHeight: 50,
                    thumbAmount: 2,
                    navigationType: 'thumb',
                    navigationArrows: 'solo',
                    navigationStyle: 'round',
                    touchenabled: 'on',
                    onHoverStop: 'on',
                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,
                    spinner: 'spinner0',
                    keyboardNavigation: 'off',
                    navigationHAlign: 'center',
                    navigationVAlign: 'bottom',
                    navigationHOffset: 0,
                    navigationVOffset: 20,
                    soloArrowLeftHalign: 'left',
                    soloArrowLeftValign: 'center',
                    soloArrowLeftHOffset: 20,
                    soloArrowLeftVOffset: 0,
                    soloArrowRightHalign: 'right',
                    soloArrowRightValign: 'center',
                    soloArrowRightHOffset: 20,
                    soloArrowRightVOffset: 0,
                    shadow: 0,
                    fullWidth: 'on',
                    fullScreen: 'off',
                    stopLoop: 'off',
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: 'off',
                    autoHeight: 'off',
                    forceFullWidth: 'on',
                    fullScreenAlignForce: 'off',
                    minFullScreenHeight: 0,
                    hideNavDelayOnMobile: 1500,
                    hideThumbsOnMobile: 'off',
                    hideBulletsOnMobile: 'off',
                    hideArrowsOnMobile: 'off',
                    hideThumbsUnderResolution: 0,
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    startWithSlide: 0,
                    fullScreenOffsetContainer: ''
                });
            });
        </script>

        <div class="our-features-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-xs-12 col-sm-6">
                        <div class="feature-box first"> <span class="fa fa-truck"></span>
                            <div class="content">
                                <h3>Delivery Within VI and Ikeja Axis </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12 col-sm-6">
                        <div class="feature-box"> <span class="fa fa-headphones"></span>
                            <div class="content">
                                <h3>24X7 Customer Support</h3>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-3 col-xs-12 col-sm-6">
                      <div class="feature-box"> <span class="fa fa-share"></span>
                        <div class="content">
                          <h3>Returns and Exchange</h3>
                        </div>
                      </div>
                    </div> -->
                    <div class="col-lg-4 col-xs-12 col-sm-6">
                        <div class="feature-box last"> <span class="fa fa-phone"></span>
                            <div class="content">
                                <h3>Hotline  +2349090551095</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <!--    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open%20Sans:400italic,400,300,600,700,800%7CYanone+Kaffeesatz:400,700,300,200%7COpen+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext" />-->
        <!--    <link rel="stylesheet" href="catalog/view/theme/kabriodemo2/stylesheet/0ea5236d6798067b16501e574061ac23.css" />-->
        <!---->
        <!---->
        <!--                                    <div class="wpsl-search wpsl-clearfix ">-->
        <!--                                        <div class="container">-->
        <!--                                            <div class="row">-->
        <!--                                                <div class="col-sm-12">-->
        <!--                                        <div id="wpsl-search-wrap">-->
        <!--                                            <form autocomplete="off">-->
        <!--                                                <div class="wpsl-input">-->
        <!--                                                    <input id="wpsl-search-input" type="text" value="" name="wpsl-search-input" placeholder="Your Location e.g. Victoria Island" aria-required="true" />-->
        <!--                                                </div>-->
        <!--                                                <div class="wpsl-search-btn-wrap"><input id="wpsl-search-btn" type="submit" value="Search"></div>-->
        <!--                                            </form>-->
        <!--                                        </div>-->
        <!--                                    </div>-->
        <!--                                            </div>-->
        <!--                                        </div>-->
        <!--                                    </div>-->


        <section class="bestseller">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- featured category fashion -->
                        <div class="category-product">
                            <div class="navbar nav-menu">
                                <div class="navbar-collapse">
                                    <div class="new_title">
                                        <h2>Best Sellers</h2>
                                        <div class="line">
                                            <div class="star"></div>
                                        </div>
                                    </div>
<!--                                    <ul class="nav navbar-nav">-->
<!--                                        <li class="active"><a data-toggle="tab" class="thicker-font" href="#bungalow_appetizer">Bungalow Appetizer</a></li>-->
<!--                                        <li><a data-toggle="tab" class="thicker-font" href="#bungalow_salad">Bungalow Salad</a></li>-->
<!--                                        <li><a data-toggle="tab" class="thicker-font" href="#nigerian_menu">Nigerian Menu</a></li>-->
<!---->
<!--                                        <li><a data-toggle="tab" class="thicker-font" href="#beef_chicken">Beef & Chicken Platters</a></li>-->
<!--                                        <li><a data-toggle="tab" class="thicker-font" href="#salted_crepes">Salted Crepes</a></li>-->
<!--                                    </ul>-->
                                </div>
                                <!-- /.navbar-collapse -->

                            </div>
                            <div class="product-bestseller">
                                <div class="product-bestseller-content">
                                    <div class="product-bestseller-list">
                                        <div class="tab-container">
                                            <!-- tab product -->
                                            <div class="tab-panel active" id="bungalow_appetizer">
                                                <div class="category-products">
                                                    <ul class="products-grid">

                                                        <?php

                                                        $retrieval = DB::getInstance()->query("SELECT * FROM bungalow_appetizers ORDER BY id DESC LIMIT 8");
                                                        if(!$retrieval->results()) {
                                                            echo 'No products are available here yet. You can contact us to enquire <a href="contact_us.php">here.</a>';
                                                        } else {
                                                            foreach($retrieval->results() as $retrieves) {
                                                                echo '<li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                                                <div class="item-inner">

                                                            <div class="item-img">
                                                                <div class="item-img-info">
                                                                    <a class="product-image" href="shop.php" title="'.$retrieves->product_name.'">
                                                                        <img src="uploads/'.$retrieves->product_image.'" alt="'.$retrieves->product_name.'" title="'.$retrieves->product_name.'"/>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="item-info">
                                                                <div class="info-inner">
                                                                    <div class="item-title">
                                                                        <a title="'.$retrieves->product_name.'" table-id="'.$retrieves->id.'" table-name="bungalow_appetizers" id="food_name" class="food_name" href="cart.php?id='.$retrieves->id.'">'.$retrieves->product_name.'</a>
                                                                    </div>

                                                                    <div class="item-content">

                                                                        <div class="item-price">
                                                                            <div class="price-box">
                                                                                <p class="special-price"><span class="price">N'.$retrieves->product_price.'</span></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="action">
                                                                           <button type="button" title="" data-original-title="Add to Cart" id="the-clicked" class="button btn-cart link-cart the-clicked" ><i class="fa fa-shopping-cart"></i></button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div> 
                                                        </div>
                                                        </li>';
                                                            }
                                                        }

                                                        ?>

                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>





        <div class="deals-of-day">
            <div class="container">
                <div class="col-md-12 col-sm-12">
                    <div class="deals-inner">
                        <h2> Today Deals</h2>
                        <div class="starSeparator">
                            <div class="line-left"></div>
                            <div class="star"></div>
                            <div class="line-right"></div>
                        </div>
                        <!--       <p>Glass Noodle Salad : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique aucto...-->

                        </p>
                        <div style="font-size: 30px; color: #fff;" class="box-timer">
                            <!--          <div class="timer-grid"  data-time="2016-02-15"></div>-->
                            <span style="font-size: 30px; color: #fff;"  id="days">00</span>:
                            <span style="font-size: 30px; color: #fff;" id="hours">00</span>:
                            <span style="font-size: 30px; color: #fff;" id="minutes">00</span>:
                            <span style="font-size: 30px; color: #fff;" id="seconds">00 </span>
                        </div>

                        <script>
                            var countdown = function(end, elements, callback){
                                var _second = 1000,
                                    _minute = _second * 60,
                                    _hour = _minute * 60,
                                    _day = _hour * 24,

                                    end = new Date(end),
                                    timer,


                                    calculate = function(){

                                        var now = new Date(),
                                            remaining = end.getTime() - now.getTime(),
                                            data;

                                        if (isNaN(end)){
                                            console.log('Invalid date/time');
                                            return;
                                        }

                                        if (remaining <= 0){
                                            clearInterval(timer);

                                            if (typeof callback === 'function'){
                                                callback();
                                            }

                                        } else{
                                            if (!timer){
                                                timer = setInterval(calculate, _second);
                                            }

                                            data = {
                                                'days': Math.floor(remaining / _day),
                                                'hours': Math.floor((remaining % _day) / _hour),
                                                'minutes': Math.floor((remaining % _hour) / _minute),
                                                'seconds': Math.floor((remaining % _minute) / _second),
                                            }

                                            if (elements.length){
                                                for (x in elements){
                                                    var x = elements[x];
                                                    data[x] = ('00' + data [x]).slice(-2);
                                                    document.getElementById(x).innerHTML = data[x];
                                                }
                                            }

                                        }


                                    };

                                calculate();
                            }

                            var callbackfunction = function(){
                                console.log('Done!');
                            }
                            countdown('03/30/2018 10:30:00 PM', ['days', 'hours', 'minutes', 'seconds'], callbackfunction);
                        </script>

                        <a href="shop.php" class="shop-now">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>



        <div class="container">
            <section class="featured-pro">
                <div class="slider-items-products">
                    <div class="featured-block">
                        <div id="featured-slider" class="product-flexslider hidden-buttons">
                            <div class="home-block-inner">
                                <div class="new_title">
                                    <h2>Featured Products</h2>
                                    <div class="line">
                                        <div class="star"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-items slider-width-col4 products-grid block-content">
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="indexa9bd.html?route=product/product&amp;product_id=1002" title="Aloo kathi roll">
                                                    <img src="image/cache/catalog/Restaurant/product2-700x850.jpg" alt="Aloo kathi roll" title="Aloo kathi roll"/>
                                                </a>
                                                <div class="sale-label sale-top-right">Sale</div>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Aloo kathi roll" href="indexa9bd.html?route=product/product&amp;product_id=1002">
                                                        Aloo kathi roll                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">

                                                            <p class="old-price"><span class="price">₦279.99</span></p>
                                                            <p class="special-price"><span class="price">₦200.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1002');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="indexb545.html?route=product/quickview&amp;product_id=1002;" class="link-quickview" data-name="Aloo kathi roll"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1002');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1002');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="index7e6d.html?route=product/product&amp;product_id=1007" title="Plate Lettuce Ketchup Meat Potatoes White">
                                                    <img src="image/cache/catalog/Restaurant/product7-700x850.jpg" alt="Plate Lettuce Ketchup Meat Potatoes White" title="Plate Lettuce Ketchup Meat Potatoes White"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Plate Lettuce Ketchup Meat Potatoes White" href="index7e6d.html?route=product/product&amp;product_id=1007">
                                                        Plate Lettuce Ketchup Mea...                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <p class="regular-price"><span class="price">₦100.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1007');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="index6de9.html?route=product/quickview&amp;product_id=1007;" class="link-quickview" data-name="Plate Lettuce Ketchup Meat Potatoes White"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1007');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1007');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="index079f.html?route=product/product&amp;product_id=1014" title="Pork fried rice chicken">
                                                    <img src="image/cache/catalog/Restaurant/product14-700x850.jpg" alt="Pork fried rice chicken" title="Pork fried rice chicken"/>
                                                </a>
                                                <div class="sale-label sale-top-right">Sale</div>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Pork fried rice chicken" href="index079f.html?route=product/product&amp;product_id=1014">
                                                        Pork fried rice chicken                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">

                                                            <p class="old-price"><span class="price">₦100.00</span></p>
                                                            <p class="special-price"><span class="price">₦90.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1014');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="indexb080.html?route=product/quickview&amp;product_id=1014;" class="link-quickview" data-name="Pork fried rice chicken"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1014');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1014');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="index3d82.html?route=product/product&amp;product_id=1012" title="Chicken Burger">
                                                    <img src="image/cache/catalog/Restaurant/product12-700x850.jpg" alt="Chicken Burger" title="Chicken Burger"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Chicken Burger" href="index3d82.html?route=product/product&amp;product_id=1012">
                                                        Chicken Burger                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <p class="regular-price"><span class="price">₦100.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1012');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="index728c.html?route=product/quickview&amp;product_id=1012;" class="link-quickview" data-name="Chicken Burger"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1012');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1012');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="indexbf49.html?route=product/product&amp;product_id=1023" title="Cream cheese brownies">
                                                    <img src="image/cache/catalog/Restaurant/product21-700x850.jpg" alt="Cream cheese brownies" title="Cream cheese brownies"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Cream cheese brownies" href="indexbf49.html?route=product/product&amp;product_id=1023">
                                                        Cream cheese brownies                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <p class="regular-price"><span class="price">₦100.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1023');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="index1a6e.html?route=product/quickview&amp;product_id=1023;" class="link-quickview" data-name="Cream cheese brownies"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1023');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1023');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="indexdf3d.html?route=product/product&amp;product_id=1009" title="Lobster">
                                                    <img src="image/cache/catalog/Restaurant/product9-700x850.jpg" alt="Lobster" title="Lobster"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Lobster" href="indexdf3d.html?route=product/product&amp;product_id=1009">
                                                        Lobster                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <p class="regular-price"><span class="price">₦100.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1009');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="indexddfd.html?route=product/quickview&amp;product_id=1009;" class="link-quickview" data-name="Lobster"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1009');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1009');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="index41da.html?route=product/product&amp;product_id=1024" title="Grilled Chicken Marinade">
                                                    <img src="image/cache/catalog/Restaurant/product22-700x850.jpg" alt="Grilled Chicken Marinade" title="Grilled Chicken Marinade"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Grilled Chicken Marinade" href="index41da.html?route=product/product&amp;product_id=1024">
                                                        Grilled Chicken Marinade                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <p class="regular-price"><span class="price">₦199.99</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1024');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="index2f3a.html?route=product/quickview&amp;product_id=1024;" class="link-quickview" data-name="Grilled Chicken Marinade"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1024');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1024');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="index4411.html?route=product/product&amp;product_id=1016" title="Ravioli">
                                                    <img src="image/cache/catalog/Restaurant/product16-700x850.jpg" alt="Ravioli" title="Ravioli"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Ravioli" href="index4411.html?route=product/product&amp;product_id=1016">
                                                        Ravioli                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <p class="regular-price"><span class="price">₦1,000.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1016');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="index728e.html?route=product/quickview&amp;product_id=1016;" class="link-quickview" data-name="Ravioli"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1016');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1016');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="index2ca3.html?route=product/product&amp;product_id=1020" title="Steak tartare">
                                                    <img src="image/cache/catalog/Restaurant/product20-700x850.jpg" alt="Steak tartare" title="Steak tartare"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Steak tartare" href="index2ca3.html?route=product/product&amp;product_id=1020">
                                                        Steak tartare                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <p class="regular-price"><span class="price">₦100.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1020');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="index3244.html?route=product/quickview&amp;product_id=1020;" class="link-quickview" data-name="Steak tartare"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1020');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1020');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item-img">
                                            <div class="item-img-info">
                                                <a class="product-image" href="indexbc3d.html?route=product/product&amp;product_id=1004" title="Spaetzle">
                                                    <img src="image/cache/catalog/Restaurant/product4-700x850.jpg" alt="Spaetzle" title="Spaetzle"/>
                                                </a>
                                                <div class="sale-label sale-top-right">Sale</div>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a title="Spaetzle" href="indexbc3d.html?route=product/product&amp;product_id=1004">
                                                        Spaetzle                </a>
                                                </div>
                                                <div class="rating">
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">

                                                    <div class="item-price">
                                                        <div class="price-box">

                                                            <p class="old-price"><span class="price">₦80.00</span></p>
                                                            <p class="special-price"><span class="price">₦70.00</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="action">
                                                        <ul class="add-to-links">
                                                            <li><button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="cart.add('1004');"><i class="fa fa-shopping-cart"></i></button></li>
                                                            <li>
                                                                <a href="index3cf3.html?route=product/quickview&amp;product_id=1004;" class="link-quickview" data-name="Spaetzle"><i class="fa fa-search"></i></a>
                                                            </li>

                                                            <li>
                                                                <a onclick="wishlist.add('1004');" class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="link-compare"  onclick="compare.add('1004');"><i class="fa fa-random"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>




        <?php

        require_once 'template/footer.php';
        require_once 'template/mobile_menu.php';
        require_once 'template/scripts.php';

        ?>
