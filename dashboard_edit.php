<?php
ob_start();

require_once 'core/init.php';
require_once 'classes/Upload.php';
require_once 'requirements.php';
require_once 'themes/dashboard/dashboard_header.php';
require_once 'themes/dashboard/dashboard_sidebar.php';
require_once 'core/connection.php';

if(!isset($_GET['id'])){
    header("Location:include/errors/404.php");
    exit;
}else{
    $Pid = intval($_GET['id']);
}

$stmt = $conn->stmt_init();

$price_array = array();$error = false; $message = array(); $success = false;
//get details of product
$sql = "SELECT a.ProductName, a.ProductDescription, a.ProductCategoryId, b.Price, b.branchId
FROM products a, branch_product b
WHERE a.Id = b.ProductId AND a.Id = ?";
$stmt->prepare($sql);
$stmt->bind_param('i',$Pid);
$stmt->bind_result($fetchedProductName, $fetchedProductDescription, $fetchedProductCategory, $fetchedProductPrice, $fetchedProductBranch);
$stmt->execute();  
$stmt->store_result();
if($stmt->num_rows == 0){
    header("Location:include/errors/404.php");
    exit;
}elseif($stmt->num_rows == 1){
    $stmt->fetch();
    $stmt->free_result();
}elseif($stmt->num_rows == 2){
    while($stmt->fetch()){
        $price_array[$fetchedProductBranch] = $fetchedProductPrice;
    }
}
$stmt->free_result();

//edit product
if(array_key_exists('add_item_submit', $_POST)){
    $name = $_POST['add_item_name'];

    $description = $_POST['add_item_description'];
    
    $category = $_POST['optionguys'];

    //$fileupload = $_FILES['add_item_image'];

    $branch = $_POST['add_item_check'];

    $branchVI = '';
    $branchIKEJA = '';

    if(empty($name)||empty($description)||$category==0||count($branch)==0){
        $error=true;
        $message[] = 'Fill up the fields';    
    }else{
        //filter branch details
        $branchcount = count($branch);
        for($i = 0;$i < $branchcount; $i++){
            if($branch[$i] == 1){
                $branchVI = $_POST['add_item_price_vi'];
                if(empty($branchVI)){$error = true; $message[] = 'Fill up the fields';}
            }elseif($branch[$i] == 2){
                $branchIKEJA = $_POST['add_item_price_ikeja'];
                if(empty($branchIKEJA)){$error = true; $message[] = 'Fill up the fields';}
            }
        }

        //if not errors, add to DB
        
            //upload image
            
            //continue insertion
            $sql = "UPDATE products SET ProductName = ?, ProductDescription = ?, ProductCategoryId = ? WHERE id = ?";
            if($stmt->prepare($sql)){
                $stmt->bind_param('ssii', $name, $description, $category, $Pid);
                $stmt->execute();
                //echo $stmt->error;
                if($stmt->affected_rows > 0){
                    $id = $stmt->insert_id;
                }else{
                    $error = true;
                    $message[] = 'Couldnt edit record';
                }
            }
            $stmt->free_result();

            //add to branch_product table
            for($i = 0;$i < $branchcount; $i++){
                if($branch[$i] == 1){
                    $sql = "UPDATE branch_product SET BranchId = ?, Price = ? WHERE ProductId = ?";
                    if($stmt->prepare($sql)){
                        $stmt->bind_param('iii', $category, $branchVI, $Pid);
                        $stmt->execute();
                        //echo $stmt->error;
                        if($stmt->affected_rows > 0){
                            $success = true;
                            
                        }else{
                            $error = true;
                            //$message[] = 'Couldnt insert record into database';
                        }
                    }
                    $stmt->free_result();        
                }elseif($branch[$i] == 2){
                    $sql = "UPDATE branch_product SET BranchId = ?, Price = ? WHERE ProductId = ?";
                    if($stmt->prepare($sql)){
                        $stmt->bind_param('iii', $category, $branchIKEJA, $Pid);
                        $stmt->execute();
                        //echo $stmt->error;
                        if($stmt->affected_rows > 0){
                            $success = true;
                        }else{
                            $error = true;
                            //$message[] = 'Couldnt insert record into database';
                        }
                    }
                    $stmt->free_result();
                }
                
            }//for    
        
    }
}//arraykeyexists

?>

<div class="col-md-6 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel-cover">
            <div class="panel-heading">
                <h2>Edit Product</h2>
                <ul>
                <?php 
                    if($_POST && $error){
                        foreach($message as $messages){ ?>
                            <li><?= $messages; ?></li>
                        <?php }
                    }elseif($_POST && $success){
                        header("Location:dashboard_products.php");
                        exit;
                    }
                ?>
                </ul>
            </div>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <form class="mdform" method="post" action="" enctype="multipart/form-data">
                <div class="form-group md-form-group">

                    <label for="form1-email">Name of Item</label>
                    <input name="add_item_name" value="<?= htmlentities($fetchedProductName); ?>" type="text" class="form-control">
                </div>
                <!-- <div class="form-group md-form-group">
                    <label for="form1-pass">Price of Item</label>
                    <input name="add_item_price" type="text" class="form-control">
                </div> -->
                <!-- <div class="form-group md-form-group">
                    <input type="file" name="add_item_image" id="imageChecker" accept=".jpg, .jpeg, .png"/>
                </div> -->
                <div class="form-group md-form-group">
                    <label for="form1-pass"></label>
                    <textarea name="add_item_description" class="form-control" placeholder="Item Description"><?= htmlentities($fetchedProductDescription); ?></textarea>
                </div>
<!--                <div class="mdl-textfield mdl"-->
                <div class="form-group md-form-group">
                    <h4>Select Product Category</h4>
                    <select name="optionguys">
                        <option value="0"><span class="label-text">Select One:</span></option>
                        <?php
                            $sql = "SELECT a.Id, a.CategoryName, b.ProductCategoryName FROM category a, subproductcategory b WHERE a.SubProductCategoryId = b.Id";
                            if($stmt->prepare($sql)){
                                $stmt->execute();
                                $stmt->bind_result($a, $b, $c);
                                $stmt->store_result();
                                $numrows = $stmt->num_rows();
                                if($numrows > 0){
                                    while($stmt->fetch()){ ?>
                                <option value="<?= $a; ?>" <?php if($a == $fetchedProductCategory){echo 'selected';} ?>>
                                <span class="label-text"><?= $b.'('.$c.')'; ?></span></option>     
                                <?php }
                                }
                            }
                        ?>
                    </select>
                </div>

                <?php if(count($price_array)== 0){ ?>
                <div class="form-group checkbox">
                    Branch
                    <div class="row">
                    <div class="form-group col-md-3">
                <input type="checkbox" name="add_item_check[]" value="1" class="icheck" <?php if($fetchedProductBranch == 1){ ?> checked="checked" <?php } ?>> 
                        <span class="label-text">VI</span>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="form1-pass">Price of Item</label>
                        <input name="add_item_price_vi" <?php if($fetchedProductBranch == 1){ ?> value="<?= $fetchedProductPrice; ?>" <?php } ?> type="text" class="form-control">
                    </div>
                    </div>

                    <div class="row">
                    <div class="form-group col-md-3">
                        <input type="checkbox" name="add_item_check[]" value="2" class="icheck" <?php if($fetchedProductBranch == 2){ ?> checked="checked" <?php } ?>> 
                        <span class="label-text">Ikeja</span>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="form1-pass">Price of Item</label>
                        <input name="add_item_price_ikeja" type="text" <?php if($fetchedProductBranch == 2){ ?> value="<?= $fetchedProductPrice; ?>" <?php } ?>  class="form-control">
                    </div>
                </div>
                <?php }elseif(count($price_array)>= 1){ ?>
                    <div class="form-group checkbox">
                    Branch
                    <div class="row">
                    <div class="form-group col-md-3">
                <input type="checkbox" name="add_item_check[]" value="1" class="icheck" <?php if(array_key_exists('1',$price_array)){ ?> checked="checked" <?php } ?>> 
                        <span class="label-text">VI</span>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="form1-pass">Price of Item</label>
                        <input name="add_item_price_vi" <?php if(array_key_exists('1',$price_array)){ ?> value="<?= $price_array[1]; ?>" <?php } ?> type="text" class="form-control">
                    </div>
                    </div>

                    <div class="row">
                    <div class="form-group col-md-3">
                        <input type="checkbox" name="add_item_check[]" value="2" class="icheck" <?php if(array_key_exists('2',$price_array)){ ?> checked="checked" <?php } ?>> 
                        <span class="label-text">Ikeja</span>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="form1-pass">Price of Item</label>
                        <input name="add_item_price_ikeja" type="text" <?php if(array_key_exists('2',$price_array)){ ?> value="<?= $price_array[2]; ?>" <?php } ?>  class="form-control">
                    </div>
                </div>
                <?php } ?>
                </div>
                <button name="add_item_submit" type="submit" class="btn btn-info">Edit</button>

            </form>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel-primary panel -->

</div>


<?php 
ob_end_flush();
?>
